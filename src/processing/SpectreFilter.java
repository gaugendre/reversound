/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing;

import conteneurs.Gamme;
import conteneurs.Spectre;

/**
 * Created by gaby on 27/04/14.
 */
public abstract class SpectreFilter {

	protected SpectreFilter previousFilter = null;
	protected float filterIntensity = 1;
	protected boolean autoIntensity = true;

	public SpectreFilter(){
		init();
	}

	protected void init(){}

	/**
	 * Renvoie l'amplitude filtré en fonction de l'indice de la fréquence dans le spectre et du spectre
	 * Cette méthode peut être hérité pour définir un filtre propre à chaque fréquence
	 * @param idFreq l'index associé à la fréquence que l'on veut filtrer
	 * @param spectre spectre d'entrée
	 * @return amplitude filtrée
	 */
	protected abstract float filter(int idFreq, Spectre spectre);


	/**
	 * Renvoie l'amplitude filtré en fonction de l'indice de la fréquence dans le spectre et du spectre
	 * @param idFreq l'index associé à la fréquence que l'on veut filtrer
	 * @param spectre spectre du signal
	 * @return l'amplitude de la fréquence filtré à travers la chaine de filtre
	 */
	public float filterAmpli(int idFreq, Spectre spectre){
		if(previousFilter==null)
			return autoIntensity?filterIntensity*filter(idFreq,spectre)+(1-filterIntensity)*spectre.getAmplitude(idFreq)/2:filter(idFreq,spectre);

		return filter(idFreq, previousFilter.filterSpectre(spectre));
	}


	/**
	 * Renvoie le spectre filtré à travers la chaine de filtre du premier maillon jusqu'à celui-ci
	 * @param spectre spectre d'entrée
	 * @return spectre filtré à travers la chaine
	 */
	public Spectre filterSpectre(Spectre spectre){
		Spectre r = new Spectre(spectre.getGamme());

		Gamme gamme = spectre.getGamme();
		if(previousFilter!=null)
			spectre = previousFilter.filterSpectre(spectre);

		for (int i = 0; i < gamme.gammeSize(); i++)
			r.setAmplitudeAt(i, autoIntensity?filterIntensity*filter(i,spectre)+(1-filterIntensity)*spectre.getAmplitude(i)/2:filter(i,spectre));

		return r;
	}

	/**
	 * Chaine un filtre à celui-ci, le filtre chainer sera executé après ce filtre
	 * @param filter le filtre à ajouter à la chaine de traitement
	 * @return la chaine de filtre
	 */
	public SpectreFilter chainFilter(SpectreFilter filter){
		filter.previousFilter = this;
		return filter;
	}


	/**
	 * Coupe la chaine à ce filtre il est maintenant le premier filtre à être executé lors du calcul de la chaine
	 * @return Vrai si il n'était pas déjà le premier maillon
	 */
	public boolean unchainFilter(){
		if(previousFilter==null)
			return false;

		previousFilter = null;
		return true;
	}

	/**
	 * Renvoie le maillon précédant dans la chaine
	 * @return
	 */
	public SpectreFilter getPreviousFilter(){
		return previousFilter;
	}

	public float getFilterIntensity() {
		return filterIntensity;
	}

	public SpectreFilter setIntensity(float filterIntensity) {
		this.filterIntensity = filterIntensity;
		return this;
	}
}
