/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui;

import generictools.Instant;
import gui.viewer_state.BufferViewerState;
import gui.viewer_state.Viewer;
import processing.ProcessControl;
import processing.buffer.TemporalBuffer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Un panel pour le contrôle du volum et du temps.
 */
public class TimeSlider extends JPanel implements PlayerControl.Listener, Viewer {

    private boolean sliderUnlocked = false;
	private final PlayerControl control = PlayerControl.instance();

	private final JSlider slider;
	private final JLabel posLabel;
    private final JButton playPause;
    private final JButton volUp = new JButton("+");
    private final JButton volDown = new JButton("-");
    private final JButton mute = new JButton("Mute");

    /**
     * Génère le panel.
     */
    public TimeSlider(){
		super();

		BoxLayout layout = new BoxLayout(this, BoxLayout.LINE_AXIS);
        this.setLayout(layout);

        playPause = new JButton("Play");
            add(playPause);

        add(volDown);
        volDown.addActionListener(e -> PlayerControl.instance().setVolume(PlayerControl.instance().getVolume() - 0.1f));

        add(volUp);
        volUp.addActionListener(e -> PlayerControl.instance().setVolume(PlayerControl.instance().getVolume() + 0.1f));

        add(mute);
        mute.addActionListener(e -> {
            if (PlayerControl.instance().isMute()) {
                PlayerControl.instance().unMute();
                mute.setText("Mute");
            }
            else {
                PlayerControl.instance().mute();
                mute.setText("Unmute");
            }
        });


		slider = new JSlider(0,1,0);
			slider.setPreferredSize(new Dimension(300,25));
			add(slider);

		posLabel = new JLabel();
			posLabel.setBorder(BorderFactory.createLoweredBevelBorder());
			posLabel.setPreferredSize(new Dimension(120,25));
			add(posLabel);


		ProcessControl p = ProcessControl.instance();
		BufferViewerState.sizeSensible(this, p.getMainInput().getBuffer()).setVisibility(true);

		control.addListener(this);
        slider.addChangeListener(e -> sliderMoved());
        slider.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
				sliderUnlocked = true;
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				sliderUnlocked = false;
			}

			@Override
			public void mouseEntered(MouseEvent e) {}

			@Override
			public void mouseExited(MouseEvent e) {}
		});

        playPause.addActionListener(e -> {
            if (playPause.getText().equals("Pause"))
                control.pause();
            else
                control.play();
        });

    }

    private void sliderMoved() {
        if(sliderUnlocked) {
			control.setFrame(Instant.fromIndex(slider.getValue(), ProcessControl.instance().getMainInput().getBuffer()));
		}
    }


	@Override
	public void playerControlEvent(PlayerControlEvent e) {
		switch (e.getType()){
			case FRAME:
				TemporalBuffer<Float> b = ProcessControl.instance().getMainInput().getBuffer();
				if(!sliderUnlocked)
					slider.setValue(e.getFrame().mapToIndex(b));

				posLabel.setText(e.getFrame()	 + " / "+ Instant.fromIndex(ProcessControl.instance().getMainInput().getBuffer().size(),44100));
				break;

			case PLAY_STATE:
				switch (e.getPlayingState()){
					case PLAYING:
						playPause.setText("Pause");
						break;
					case PAUSED:
						playPause.setText("Play");
						break;
					case STOPPED:
						playPause.setText("Play");
						break;
				}
				break;

			case MUTE_STATE:
				if(e.isMuted())
					mute.setText("Unmute");
				else
					mute.setText("Mute");
				break;
		}	}

	@Override
	public void updateView() {
		slider.setMaximum(ProcessControl.instance().getMainInput().getBuffer().size());
		posLabel.setText(posLabel.getText().substring(0, posLabel.getText().indexOf("/")+1)+ Instant.fromIndex(ProcessControl.instance().getMainInput().getBuffer().size(), 44100));

		if(!slider.isEnabled()) {
			slider.setEnabled(true);
			PlayerControl.instance().setFrame(Instant.fromTime(0));
		}
	}

	@Override
	public void cleanView() {
		slider.setMaximum(1);
		slider.setEnabled(false);
		posLabel.setText(" -- / --");
	}
}
