/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package generictools;

/**
 * Created by Gabriel on 25/03/2014.
 * Permet de créer des nombres complexes, utiles pour la FFT notamment
 */
public class Complex {

	private float realPart;
	private float imagPart;
	public Complex (float realPart, float imagPart){
		this.realPart = realPart;
		this.imagPart = imagPart;
	}

	public Complex( Complex c){
		this(c.getRealPart(), c.getImagPart());
	}

	/**
	 * Transforme un complexe sous la forme (module, argument) en (réel, imaginaire).
	 * @param mod Le module du complexe à transformer
	 * @param arg L'argument du complexe à transformer
	 * @return Le complexe nouvellement formé
	 */
	public static Complex ComplexFromExp(float mod, float arg){
		return new Complex( mod*(float)Math.cos(arg), mod*(float)Math.sin(arg));
	}

	/**
	 * Calcule le conjugué d'un complexe.
	 * @return Le conjugué du complexe actuel (this)
	 */
	public Complex conj(){
		return new Complex(this.realPart, (-1f*this.imagPart) );
	}

	/**
	 * Somme deux complexes.
	 * @param c2 Le complexe auquel ajouter le complexe actuel (this)
	 * @return La somme des deux complexes
	 */
	public Complex sum(Complex c2){
		return new Complex(realPart+c2.getRealPart(), imagPart+c2.getImagPart());
	}

	/**
	 * Soustrait c2 au complexe actuel (this).
	 * @param c2 Le nombre à ôter
	 * @return Le complexe résultant de la soustraction
	 */
	public Complex sub(Complex c2){
		return new Complex(realPart-c2.getRealPart(), imagPart-c2.getImagPart());
	}

	/**
	 * Multiplie deux complexes entre eux.
	 * @param c2 Le complexe par lequel multiplier le complexe actuel (this)
	 * @return Le produit des deux complexes
	 */
	public Complex mult(Complex c2){
		return new Complex( ((realPart * c2.getRealPart()) - (imagPart * c2.getImagPart())), ((realPart*c2.getImagPart()) + (c2.getRealPart()*imagPart)) );
	}

	/**
	 * Prend le carré de ce complex;
	 * @return Le produit des deux complexes
	 */
	public Complex square(){
		return new Complex((realPart-imagPart)*(realPart+imagPart), 2*imagPart*realPart);
	}
	/** Renvoie la somme, en modifiant directement le complexe appelé */
	public Complex simpleSum(Complex c2){
		realPart += c2.getRealPart();
		imagPart += c2.getImagPart();
		return this;
	}
	/** Renvoie la multiplication, en modifiant directement le complexe appelé */
	public Complex simpleMult(Complex c2){
		float tmp = realPart;
		realPart = ((realPart * c2.getRealPart()) - (imagPart * c2.getImagPart()));
		imagPart = ((tmp*c2.getImagPart()) + (c2.getRealPart()*imagPart));
		return this;
	}

	/** Permet de faire une multiplication de l'objet par lui-meme sans recreer une instance de Complexe */
	public void simpleSquare(){
		float tmp = realPart;
		realPart = (realPart-imagPart)*(realPart+imagPart);
		imagPart = 2*imagPart*tmp;
	}


	public float getRealPart(){
		return this.realPart;
	}
	public float getImagPart(){
		return this.imagPart;
	}

	/** Definit ce complexe egal a celui donne en paramètre */
	public void setToEquals(Complex c){
		this.realPart = c.getRealPart();
		this.imagPart = c.getImagPart();
	}

	/** Renvoie le module du complexe */
	public float getMod(){
		return (float)Math.sqrt((this.realPart*this.realPart)+(this.imagPart*this.imagPart));
	}
	/** Renvoie l'argument du complexe */
	public float getArg(){
		return (float)Math.atan(this.imagPart/this.realPart);
	}

}
