/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

import generictools.Instant;
import processing.buffer.NoteBuffer;

/**
 * Created by gaby on 02/06/14.
 */
public class Note extends ObservableObject {

	private int noteID;
	private int bufferID = -1;
	private Instant start;
	private Instant  end;
	private NoteBuffer buffer;

	//Caractérise la durée de la note (en diviseur de la ronde: 1 pour ronde, 2 pour blanche, 4 pour noire ...)
	private String unifiedDuration = null;

	public Note(int noteID, Instant start, Instant end) {
		this.noteID = noteID;
		this.start = start;
		this.end = end;
	}

	/** Renvoie l'instant de départ de la note
	 * @return l'instant sous forme d'Instant.  */
	public Instant getStart() {
		return start;
	}

	/** Définit l'instant de départ d'une note */
	public void setStart(Instant start) {
		this.start = start;
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
	}

	/** Permet de récupérer l'instant de fin de la note */
	public Instant getEnd() {
		return end;
	}

	/** Redefinit l'instant de fin de la note
	 * @param end l'instant de fin */
	public void setEnd(Instant end) {
		this.end = end;
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
	}

	/** Permet de récupérer l'index de la note (exemple: la 49 ... )*/
	public int getNoteID() {
		return noteID;
	}


	/** @return le nom de la note*/
	public String getNoteName(){
		return NoteGamme.getNoteName(noteID);
	}

	/**@return le nom anglais de la note*/
	public String getEnglishNoteName(){return NoteGamme.getEnglishNoteName(noteID);}

	/** @return la fréquence de la note*/
	public float getFreq(){
		return NoteGamme.getStatFreq(noteID);
	}

	/** @return Identifiant de la note dans le buffer de note */
	public int getBufferID() {
		return bufferID;
	}

	public void setBufferID(int bufferID) {
		this.bufferID = bufferID;
		emit(ObservableObjectEvent.Type.ID_CHANGED);
	}

	public NoteBuffer getBuffer() {
		return buffer;
	}

	public void setBuffer(NoteBuffer buffer) {
		this.buffer = buffer;
		emit(ObservableObjectEvent.Type.ID_CHANGED);
	}

	public String getUnifiedDuration() {
		return unifiedDuration;
	}

	public void setUnifiedDuration(String unifiedDuration) {
		this.unifiedDuration = unifiedDuration;
	}

	public float getDuration(){
		return end.substract(start);
	}

	@Override
	public Class getType() {
		return Note.class;
	}
}
