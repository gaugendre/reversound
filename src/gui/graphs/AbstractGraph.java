/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.graphs;

import conteneurs.ObservableObject;
import javafx.application.Platform;
import javafx.scene.Parent;
import processing.buffer.Buffer;

import javax.swing.*;

/**
 * Created by gaby on 07/05/14.
 * Classe abstraite définissant les attributs et méthode communes au graph
 */
public abstract class AbstractGraph {

	protected final String name;
	private final Class type;
	private  GraphicView graphicView;
	private boolean visibility = false;

	protected AbstractGraph(String name, Class type) {
		this.name = name;
		this.type = type;
	}

	/**
	 * Doit renvoyer le noeud JavaFX élémentaire du graph
	 * @return Le noeud parent
	 */
	public abstract Parent getMainNode();

	/**
	 * Revoie le liste des éléments du menu contextuel
	 * @return
	 */
	public void createContextMenu(JPopupMenu contextMenu){
		try {
			contextMenu.remove(1);
		}catch (IllegalArgumentException e){

		}
	}

	/**
	 * Vérifie que le graph est valide (qu'il n'a pas été construit via un contructeur)
	 * @return Vrai si valide
	 */
	public abstract boolean isValid();

	public void setVisibility(boolean isVisible){
		visibilityChange(isVisible);
		visibility = isVisible;
	}

	public boolean visibility(){
		return this.visibility;
	}

	protected abstract void visibilityChange(boolean isVisible);

	protected void setupView(){
		AbstractGraph thisGraph = this;
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				initView();

//				getMainNode().setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
//					@Override
//					public void handle(ContextMenuEvent e) {
//						graphicView.popupContextMenu(Math.round((float)e.getX()),Math.round((float)e.getY()),thisGraph);
//					}
//				});
			}
		});
	}
	protected abstract void initView();

	abstract public AbstractGraph setupGraph(Buffer b, GraphicView graphicView, boolean syncWithPlayer);
	abstract public AbstractGraph setupGraph(ObservableObject o, GraphicView graphicView);

	public String getName() {
		return name;
	}

	public Class getType() {
		return type;
	}

	public GraphicView getGraphicView() {
		return graphicView;
	}
	protected void setGraphicView(GraphicView graphicView){this.graphicView = graphicView;}
}
