/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package generictools;

/** Created by Gabriel Augendre on 18/04/2014.
 * Renvoie la chaine de caractere la plus longue parmi celles donnees en paramètre */
public class Strings {
    public static String longestString(String s1, String s2, String... sk) {
        if (sk.length == 0) {
            if (s1.length() > s2.length())
                return s1;
            return s2;
        } else {
            String[] array = new String[sk.length+2];
            int max = array[0].length();
            int maxIndex = 0;
            for (int i = 0; i < array.length; i++) {
                if (array[i].length() > max) {
                    max = array[i].length();
                    maxIndex = i;
                }
            }
            return array[maxIndex];
        }
    }
}
