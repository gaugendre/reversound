/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui;

import generictools.Instant;

/**
 * Created by gaby on 08/05/14.
 */
public class PlayerControlEvent {
	public enum Type{
		FRAME,
		PLAY_STATE,
		VOLUME,
		MUTE_STATE
	}
	private final Type type;

	private final Instant frame;
	private final PlayerControl.PlayingState playingState;
	private final float volume;
	private final boolean isMuted;

	public PlayerControlEvent(Type type, Instant frame, PlayerControl.PlayingState playingState, float volume, boolean isMuted) {
		this.type = type;
		this.frame = frame;
		this.playingState = playingState;
		this.volume = volume;
		this.isMuted = isMuted;
	}
	public PlayerControlEvent(Type type){
		this.type = type;
		frame = PlayerControl.instance().getFrame();
		playingState = PlayerControl.instance().getPlayingState();
		volume = PlayerControl.instance().getVolume();
		isMuted = PlayerControl.instance().isMute();
	}


	public Type getType() {
		return type;
	}

	public Instant getFrame() {
		return frame;
	}

	public PlayerControl.PlayingState getPlayingState() {
		return playingState;
	}

	public float getVolume() {
		return volume;
	}

	public boolean isMuted() {
		return isMuted;
	}
}
