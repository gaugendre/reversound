/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.properties;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.util.ArrayList;

/**
 * Created by gaby on 27/05/14.
 */
public class StringProperty extends Property<String> {
	ArrayList<TextField> textFields = new ArrayList<>();

	public StringProperty(String name, String value) {
		super(name, String.class, value);
	}

	public StringProperty(String name){
		this(name, "");
	}

	/**
	 * Retourne l'élément javaFX de modification de la propriété
	 *
	 * @return L'élément graphique lié à la propriété
	 */
	@Override
	public Control createEditNode() {
		TextField textField = new TextField(getValue());
		textField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				if(getValue()!=textField.getText())
					nodeValueModified(textField.getText());

			}
		});
		textFields.add(textField);
		return textField;
	}

	/**
	 * Appeler lorsque la proriété est modifier par autre chose que le l'élément graphique,
	 * il faut donc mettre à jour se dernier
	 */
	@Override
	protected void syncNodesValue() {
		for(TextField t:textFields)
			t.setText(getApparentValue());
	}
}
