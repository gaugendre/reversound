/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui;

import javafx.geometry.Point2D;
import javafx.scene.shape.Line;

/**
 * Permet de connecter deux Box afin de voir les liens de successions dans les Process.
 */
public class Connector extends Line {
    private Connector(double startX, double startY, double endX, double endY) {
        super(startX, startY, endX, endY);
    }

    /**
     * Crée une ligne entre deux noeuds de la fenêtre principale.
     * @param startBox La box à relier à la seconde.
     * @param endBox La seconde box à relier.
     */
    public Connector(Box startBox, Box endBox) {
        final Point2D start = startBox.localToScene(Box.NODE_WIDTH,Box.NODE_HEIGHT/2);
        final Point2D end = endBox.localToScene(0.0,Box.NODE_HEIGHT/2);
        new Connector(start.getX(), start.getY(), end.getX(), end.getY());
    }
}
