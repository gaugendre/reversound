/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package generictools;

import processing.buffer.Buffer;
import processing.buffer.TemporalBuffer;

/**
 * Created by gaby on 15/04/14.
 *
 * Instant
 *
 * Cette classe permet de repérer de manière absolue un instant dans le fichier traiter
 * Il permet ensuite de convertir cette instant en index de TemporalBuffer selon la fréquence d'échantillonage
 * et l'instant d'origine de ce dernier.
 */
public class Instant {
	private float timeMS=0;

	/**
	 * Construit un instant en le définissant par un temps dans le buffer de référence
	 * @param time temps en ms.
	 */
	public Instant(float time){
		timeMS = time;
	}

	/**
	 * Construit un instant en le définissant par un temps dans le buffer de référence
	 * @param ms temps en ms
	 * @return Instant correspondant
	 */
	public static Instant fromTime(float ms){return new Instant(ms);}

	/**
	 * Construit un instant en le définissant par un temps dans le buffer de référence
	 * @param ms temps en ms
	 * @param s temps en s
	 * @param min temps en min
	 * @param h temps en heure
	 * @return Instant correpsondant
	 */
	public static Instant fromTime(float ms, int s, int min, int h){return new Instant(ms+1000*(s+60*(min+60*h)));}

	/**
	 * Construit un instant en le définissant par un temps dans le buffer de référence
	 * @param ms temps en ms
	 * @param s temps en s
	 * @param min temps en min
	 * @return Instant correpsondant
	 */
	public static Instant fromTime(float ms, int s, int min){ return fromTime(ms,s,min,0);}

	/**
	 * Construit un instant en le définissant par un temps dans le buffer de référence
	 * @param ms temps en ms
	 * @param s temps en s
	 * @return Instant correpsondant
	 */
	public static Instant fromTime(float ms, int s){ return fromTime(ms, s, 0, 0);}


	/**
	 * Construit un instant en le définissant par un  index, un fréquence d'échantillonage,
	 * l'instant d'origine est confondu avec celui du buffer de référence
	 * @param index index de l'instant
	 * @param sampleRate fréquence d'échantillonage
	 * @return Instant correspondant
	 */
	public static Instant fromIndex(int index, int sampleRate){
		return new Instant(1000f*(float)index/(float)sampleRate);
	}

	/**
	 * Construit un instant en le définissant par un  index, un fréquence d'échantillonage et un instant d'origine
	 * @param index index de l'instant
	 * @param sampleRate fréquence d'échantillonage
	 * @param originInstant Instant d'origine
	 * @return Instant correspondant
	 */
	public static Instant fromIndex(int index, int sampleRate, Instant originInstant){
		return fromIndex(index,sampleRate).add(originInstant);
	}

	/**
	 * Construit un instant en le définissant par un  index, et un temporal buffer
	 * @param index index de l'instant
	 * @param temporalBuffer buffer temporel dans lequel on désire connaitre l'instant
	 * @return Instant correspondant
	 */
	public static Instant fromIndex(int index, TemporalBuffer temporalBuffer){ return fromIndex(index, temporalBuffer.getSampleRate(), temporalBuffer.getOriginInstant());}

	/**
	 * Construit un instant en le définissant par un  index, et un buffer qui est implicitement converti en buffer temporel,
	 * si le buffer n'est pas un buffer temporel renvoit Instant(0)
	 * @param index index de l'instant
	 * @param buffer buffer temporel dans lequel on désire connaitre l'instant
	 * @return Instant correspondant ou Instant(0)
	 */
	public static Instant fromIndex(int index, Buffer buffer){
		if(buffer instanceof TemporalBuffer)
			return fromIndex(index, (TemporalBuffer)buffer);
		return new Instant(0);
	}

	/**
	 * Récupère la durée en ms séparant ce buffer de celui passé en argument
	 * @param t Instant à comparer
	 * @return durée
	 */
	public float substract(Instant t){
		return timeMS - t.timeMS;
	};


	/**
	 * Compare cet instant avec celui passé en argument
	 * @param t instant à comparer
	 * @return Vrai si cet instant est plus grand que t
	 */
	public boolean isGreaterThan(Instant t){
		return timeMS>t.timeMS;
	}

	/**
	 * Compare cet instant avec celui passé en argument
	 * @param t instant à comparer
	 * @return Vrai si cet instant est plus petit que t
	 */
	public boolean isLowerThan(Instant t){
		return timeMS<t.timeMS;
	}

	/**
	 * Compare cet instant avec celui passé en argument
	 * @param t instant à comparer
	 * @return Vrai si cet instant est plus grand ou égal que t
	 */
	public boolean isGreaterOrEquThan(Instant t){
		return timeMS>=t.timeMS;
	}

	/**
	 * Compare cet instant avec celui passé en argument
	 * @param t instant à comparer
	 * @return Vrai si cet instant est plus petit ou égal que t
	 */
	public boolean isLowerOrEquThan(Instant t){
		return timeMS<=t.timeMS;
	}

	/**
	 * Compare cet instant avec celui passé en argument
	 * @param t instant à comparer
	 * @return Vrai si cet instant est égal à t
	 */
	public boolean equals(Object t){
		if(t instanceof Instant)
			return equals((Instant)t);
		return false;
	}

	/**
	 * Compare cet instant avec celui passé en argument
	 * @param t instant à comparer
	 * @return Vrai si cet instant est égal à t
	 */
	public boolean equals(Instant t){
		return timeMS == t.timeMS;
	}


	/**
	 * Translate cet instant d'un temps en ms /!\ Modifie cet instant
	 * @param ms temps à translater
	 * @return cet instant
	 */
	public Instant translate(float ms){
		timeMS += ms;
		return this;
	}

	/**
	 * Renvoie un instant translate d'un temps en ms, sans modifier cet instant
	 * @param ms temps à translater
	 * @return l'instant translater
	 */
	public Instant translated(float ms){
		return new Instant(timeMS+ms);
	}

	public String toString(){
		return   (int)Math.floor(timeMS/60000) + ":" +(int)Math.floor(timeMS%60000/1000)+ ":" +Math.round(timeMS%1000);
	}


	int mapToIndex(int sampleRate){
		return mapToIndex(sampleRate, new Instant(0));
	}

	/**
	 * Retourne l'index correspondant à cet Instant en fonction de la fréquence d'échantillonage et de l'instant d'origine
	 * @param sampleRate Fréquence d'échantillonage
	 * @param originInstant Instant d'origine
	 * @return index correspondant
	 */
	int mapToIndex(int sampleRate, Instant originInstant){ return Math.round((timeMS-originInstant.timeMS)*(float)sampleRate/1000f);}

	/**
	 * Retourne l'index correspondant à cet Instant dans un buffer temporel
	 * @param temporalBuffer buffer temporel
	 * @return index correspondant
	 */
	public int mapToIndex(TemporalBuffer temporalBuffer){
		return mapToIndex(temporalBuffer.getSampleRate(), temporalBuffer.getOriginInstant());
	}

	/**
	 * Retourne le temps de l'instant dans le buffer de référence
	 * @return temps en ms
	 */
	public float toMs(){
		return timeMS;
	}

	/**
	 * Ajoute à cet instant un autre instant /!\ Cet objet est modifié
	 * @param i2 instant à ajouter
	 * @return cet instant
	 */
	public Instant add(Instant i2){
		timeMS += i2.timeMS;
		return this;
	}

	/**
	 * Retire de cet instant un autre instant /!\ Cet objet est modifié
	 * @param i2 instant à ajouter
	 * @return cet instant
	 */
	public Instant remove(Instant i2){
		timeMS -= i2.timeMS;
		return this;
	}


}