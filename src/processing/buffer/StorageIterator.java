/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.buffer;

/**
 * Created by gaby on 31/03/14.
 */
public interface StorageIterator<T> {
	/**
	 * Renvoie la valeur de l'élément suivant, l'iterator ce décale alors à cet élément
	 * @return
	 */
	public T next();

	/**
	 * Teste si il reste un élément disponible après l'élément actuel
	 * @return Vrai si on peut appeler next()...
	 */
	public boolean hasNext();


}
