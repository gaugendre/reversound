/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.properties;

import generictools.Pair;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;

import java.util.ArrayList;

/**
 * Created by gaby on 27/05/14.
 */
public class ChoiceProperty<T> extends Property<T>{
	ArrayList<Pair<T, String>> choices;
	private int currentID = 0;
	ArrayList<ComboBox<String>> comboBoxs = new ArrayList<>();

	public ChoiceProperty(String name, ArrayList<Pair<T,String>> choices, Class type, int defaultIndex) {
		super(name, type, choices.get(defaultIndex).first());
		currentID = defaultIndex;
		this.choices = choices;
	}

	public ChoiceProperty(String name, ArrayList<Pair<T, String>> choices, Class type) {
		this(name, choices, type, 0);
	}

	/** Modifie la valeur associée à chaque choix possible
	 * @param choices l'ensemble des choix possibles sous forme d'ArrayList de Pair	 */
	public void forceChoices(ArrayList<Pair<T,String>> choices){
		if(choices.size()==0)
			return;

		if(getSelectedItemIndex()>=choices.size())
			setSelectedItem(0);

		this.choices = choices;

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				for(ComboBox<String> c: comboBoxs){
					c.getItems().clear();
					for(Pair<T, String> choice : choices)
						c.getItems().add(choice.second());
				}
			}
		});

		setSelectedItem(getSelectedItemIndex());
	}

	/** Renvoie l'indice de l'élément qui est selectionné en ce moment */
	public int getSelectedItemIndex(){
		return currentID;
	}

	/** Renvoie le nom de l'élément sélectionné en ce moment */
	public String getSelectedItemName(){
		return choices.get(currentID).second();
	}

	/** Redéfinit la valeur actuelle
	 * @param item le nouvel élément
	 * @return vrai si ça a fonctionné, faux sinon */
	public boolean setValue(T item){
		if(item == getValue())
			return false;

		int id = 0;
		for(id=0; id<choices.size(); id++){
			if(choices.get(id).first()==item)
				break;
		}

		if(id==choices.size())
			choices.add(new Pair<T,String>(item,item.toString()));

		return setSelectedItem(id);
	}

	/** Rétrocontrole sur l'interface
	 * @param index l'indice de l'item sélectionné
	 * @return vrai si ça a fonctionné, faux sinon */
	public boolean setSelectedItem(int index){
		if(index<0 || index >= choices.size())
			return false;

		currentID = index;
		return super.setValue(choices.get(index).first());
	}


	/**
	 * Retourne l'élément javaFX de modification de la propriété
	 *
	 * @return L'élément graphique lié à la propriété
	 */
	@Override
	public Control createEditNode() {

		ComboBox<String> comboBox = new ComboBox<>();

		for(Pair<T, String> c : choices)
			comboBox.getItems().add(c.second());

		comboBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				if(comboBox.getSelectionModel().getSelectedIndex() != getSelectedItemIndex())
					setSelectedItem(comboBox.getSelectionModel().getSelectedIndex());
			}
		});
		comboBox.getSelectionModel().select(getSelectedItemIndex());
		comboBoxs.add(comboBox);
		return comboBox;
	}

	/**
	 * Appeler lorsque la proriété est modifier par autre chose que le l'élément graphique,
	 * il faut donc mettre à jour se dernier
	 */
	@Override
	protected void syncNodesValue() {
		for(ComboBox<String> c: comboBoxs)
			c.getSelectionModel().select(getSelectedItemIndex());
	}
}
