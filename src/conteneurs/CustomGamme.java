/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;


import java.util.ArrayList;

/**
 * CustomGamme permet de créer une gamme en ne sélectionnant uniquement les fréquences qui nous intéressent.
 */
public class CustomGamme extends Gamme{
    private final ArrayList<Float> freqID;

    public CustomGamme(ArrayList<Float> listeFreq) {
        freqID = listeFreq;
    }

    /** Cherche l'indice d'une fréquence donnée en paramètre. pour une gamme quelconque. La méthode de recherche des indices les plus proches utilise la dichotomie.
     * @param freq la fréquence donnée en paramètre
     * @return l'indice de la fréquence que l'on cherche */
    public ArrayList<Number> getIndex (float freq) {
		int indexPrec = 0;
		int indexSuiv = this.freqID.size()-1;
		int newIndex = 0;
		ArrayList<Number> res = new ArrayList<>();


		// Gère le cas où on demande une fréquence qui n'est pas comprise entre le minimum et le maximum des fréquences de cette gamme.
		if(freq > this.freqID.get(this.freqID.size()-1) || freq < this.freqID.get(0)){
			res.add(-1);
			return res;
		}


		//Trouve les 2 indices qui encadrent la fréquence
		while( (indexSuiv-indexPrec) > 1){
			newIndex = Math.round((indexSuiv + indexPrec)/2);
			// System.out.println("test : "+newIndex);
			if(freq > this.getFreq(newIndex)){
				indexPrec = newIndex;
			} else {
				indexSuiv = newIndex;
			}
		}

		/** Retourne un seul indice si une des fréquences est exactement une de la liste. Sinon, retourne les deux indices les plus proches */
		if(freq == this.getFreq(indexSuiv)){
			res.add(indexSuiv);
		} else if(freq == this.getFreq(indexPrec)){
			res.add(indexPrec);
		} else {
			res.add(indexPrec);
			res.add(indexSuiv);
			float coeff = (freq - this.getFreq(indexPrec)) / (this.getFreq(indexSuiv)-this.getFreq(indexPrec));
			res.add(coeff);
		}
		return res;
    }

	/** Renvoie le nombre de fréquences que contient la gamme */
	public int gammeSize(){
		return this.freqID.size();
	}

    /** Renvoie la fréquence à l'indice donné dans la liste. La fréquence est stockée en Hz
     * @param index l'indice de la fréquence à renvoyer
     * @return la fréquence en Hz */
    public float getFreq (int index) {
        return this.freqID.get(index);
    }
}
