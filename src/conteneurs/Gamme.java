/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

import java.util.ArrayList;

public abstract class Gamme {
    /** Renvoie l'indice associé à une fréquence donnée
     * Si jamais la fréquence n'existe pas, on prend la plus proche */
    public abstract ArrayList <Number> getIndex(float freq);

    /** Renvoie la fréquence associée à un indice donné */
    public abstract float getFreq(int index);

	/** Renvoie le nombre de fréquences que contient la gamme */
	public abstract  int gammeSize();


	public ArrayList<String> toStringList(){
		ArrayList<String> r = new ArrayList<>(gammeSize());
		for (int i = 0; i < gammeSize(); i++)
			r.add(String.valueOf(getFreq(i)));

		return r;
	}
}
