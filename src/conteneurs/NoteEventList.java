/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

import java.util.ArrayList;

/**
 * Created by gaby on 21/05/14.
 * Contient une liste d'evenement de notes, a savoir une liste d'apparitions et de disparitions de notes
 */
public class NoteEventList extends ObservableList<NoteEvent> {
	public NoteEventList() {
	}

	/** Renvoie l'evenement associé à la note donnee en paramètre
	 * @param noteID l'identifiant de la note
	 * @return l'evenement sous forme de NoteEvent */
	public NoteEvent getByNoteId(int noteID){
		int id = 0;
		for(id = 0; id < size(); id++)
			if(get(id).getNote().getIdNote()==noteID)
				break;
		if(id==size())
			return null;

		return get(id);
	}

	/** Renvoie la gamme dans laquelle les notes associées aux evenements de cette liste se trouvent */
	public Gamme getEventsGamme(){
		ArrayList<Float> list = new ArrayList<>(size());
		for (int i = 0; i < size(); i++)
			list.add(get(i).getNote().getFreq());

		return new CustomGamme(list);
	}

	@Override
	public void add(NoteEvent observableItem) {
		int addId = 0;
		float freq = observableItem.getNote().getFreq();
		for(addId=size()-1; addId>=0; addId--){
			if(get(addId).getNote().getFreq() < freq){
				break;
			}
		}
		list.add(addId+1, observableItem);


	}
}
