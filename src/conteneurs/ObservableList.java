/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

import java.util.ArrayList;

/**
 * Created by gaby on 14/05/14.
 * Liste écoutable d'objet écoutable
 *
 * Cet objet est l'adaptation d'une ArrayList classique à laquelle on ajoute la gestion évenementielle lorsqu'on lui
 * ajoute ou retire des échantillons ou quand ceux ci sont modifiés.
 */
public class ObservableList<T extends ObservableObject> extends ObservableObject implements ObservableObject.Listener {

	ArrayList<T> list = new ArrayList<>();

	public ObservableList() {

	}

	/**
	 * Ajoute un objet à la liste écoutable
	 * @param observableItem objet écoutable à ajouter
	 */
	public void add(T observableItem){
		list.add(observableItem);
		observableItem.addListener(this);
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
	}

	/**
	 * Remplace un objet dans la liste
	 * @param idItem identifiant de l'objet à remplacer
	 * @param observaleItem nouvelle objet écoutable
	 * @return vrai si idItem est un identifiant correct
	 */
	public boolean set(int idItem, T observaleItem){
		if(!isIdCorrect(idItem))
			return false;

		list.set(idItem, observaleItem).removeListener(this);
		observaleItem.addListener(this);
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
		return true;
	}

	/**
	 * Supprime un objet de la liste
 	 * @param observableItem Objet observable à supprimer
	 * @return Vrai si l'objet était contenu dans la liste et a été supprimé
	 */
	public boolean remove(T observableItem){
		return remove(list.indexOf(observableItem));
	}

	/**
	 * Supprime un objet de la liste
	 * @param idItem Identifiant de l*bjet observable à supprimer
	 * @return Vrai si l'objet était contenu dans la liste et a été supprimé
	 */
	public boolean remove(int idItem){
		if(!isIdCorrect(idItem))
			return false;

		list.remove(idItem).removeListener(this);
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
		return true;
	}

	/**
	 * Retourne l'objet correspondant à l'index
	 * @param idItem Index de l'objet à récupérer
	 * @return L'objet observable correpondant. /!\ Peut renvoyer null /!\
	 */
	public T get(int idItem){
		if(!isIdCorrect(idItem))
			return null;

		return list.get(idItem);
	}


	/**
	 * Retourne la taille de la liste
	 * @return Taille
	 */
	public int size(){
		return list.size();
	}


	private boolean isIdCorrect(int id){
		return id>=0&&id<list.size();
	}

	/**
	 * Propage l'évenement e losrque un item de la liste a été modifié
	 * @param e
	 */
	@Override
	public void observableOjectEvent(ObservableObjectEvent e) {
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
	}

	@Override
	public Class getType() {
		return ObservableList.class;
	}
}
