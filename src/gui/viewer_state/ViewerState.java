/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.viewer_state;

import processing.ProcessControl;

/**
 * Created by gaby on 06/05/14.
 * Classe abstraite définissant les attributs communs au viewerState: relation avec les Graphics View
 */
public abstract class ViewerState {

	public enum State{
		UPDATE_NEEDED,
		UPDATED,
		CLEAN_NEEDED
	}
	private State state = State.CLEAN_NEEDED;

	final protected Viewer viewer;
	protected boolean visibility;
	private int slowingUpdate = 0;
	private int slowingFactorOnDataChange = 0;

	protected ViewerState(Viewer viewer) {
		this.viewer = viewer;
		visibility = false;
	}

	protected void init(){
		ProcessControl.instance().registerViewerState(this);
		setVisibility(false);
	}

	public void invalidate(){
		if(!canDraw())
			state = State.CLEAN_NEEDED;
		else
			state = State.UPDATE_NEEDED;
	}

	public void applyState(){
		if(slowingUpdate>0) {
			slowingUpdate--;
			return;
		}else if(slowingUpdate == -1)
			slowingUpdate = 0;

		if(state == State.UPDATED)
			return;
		State sCopy = state;
		state = State.UPDATED;
		if(sCopy == State.UPDATE_NEEDED)
			viewer.updateView();
		else if(sCopy== State.CLEAN_NEEDED)
			viewer.cleanView();
	}

	public boolean isVisible() {
		return visibility;
	}

	public void setVisibility(boolean visibility) {
		if(visibility==this.visibility)
			return;

		this.visibility = visibility;
		visibilityChanged();
	}

	public Viewer getViewer() {
		return viewer;
	}

	public State getState() {
		return state;
	}

	final public boolean setState(State state){
		return setState(state, 0);
	}

	final public boolean setState(State state, int slowFactor){
		if(slowFactor!=0){
			if(slowFactor==-1)
				slowingUpdate = -1;
			else if(slowingUpdate==0)
				slowingUpdate = slowFactor;
		}
		if(state == State.UPDATED)
			return this.state == State.UPDATED;

		this.state = state;
		return true;
	}

	public void forceState(State state){
		this.state = state;
		slowingUpdate = -1;
	}

	abstract protected boolean canDraw();

	protected void visibilityChanged(){

	}

	public int slowingFactorOnDataChange() {
		return slowingFactorOnDataChange;
	}

	public void setSlowingFactorOnDataChange(int slowingFactorOnDataChange) {
		this.slowingFactorOnDataChange = slowingFactorOnDataChange;
	}
}
