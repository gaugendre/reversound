/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui;

import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import processing.processes.Process;
import processing.processes.ProcessEvent;

/**
 * Définit la box d'un process qui aura des contrôles particuliers et différents d'une InputBox
 */
public class ProcessBox extends Box implements Process.Listener {
    private final Process<?> process;
    private final Button startStopButton = new Button();
    private final ProgressIndicator spinningWheel = new ProgressIndicator(0);
    private final ProgressBar progressBar = new ProgressBar(0);
    private final Button recomputeButton = new Button();

    private final String playChar = "▶";
    private final String pauseChar = "||";
    private final String resetChar = "↻";

    private boolean pauseState = false;
    private boolean waitingState = false;
    private boolean progressState = false;
    private boolean nameState = false;
    private boolean interruptState = false;

    private GridPane gridPane = new GridPane();
    private ScrollPane scrollPane = new ScrollPane(gridPane);

    /**
     * Constructeur.
     * @param process Le processus à associer à la box.
     */
    public ProcessBox(Process process) {
        super(Color.LIGHTYELLOW, new Text(process.getName()));
        this.process = process;

        super.titre.setX(70);

        //On s'occupe d'abord du bouton de play/pause du process
        startStopButton.setText(playChar);
        this.getChildren().add(startStopButton);
        startStopButton.setTranslateX(5);
        startStopButton.setTranslateY(5);
        startStopButton.setPrefWidth(25);
        startStopButton.setMaxWidth(25);
        startStopButton.setOnAction(actionEvent -> {
            if (startStopButton.getText().equals(playChar)) {
                process.start();
                startStopButton.setText(pauseChar);
            }
            else {
                process.pause();
                startStopButton.setText(playChar);
            }
        });
        process.addListener(this);

        recomputeButton.setText(resetChar);
        this.getChildren().add(recomputeButton);
        recomputeButton.setTranslateX(35);
        recomputeButton.setTranslateY(5);
        recomputeButton.setPrefWidth(25);
        recomputeButton.setMaxWidth(25);
        recomputeButton.setOnAction(actionEvent -> process.recomputeAll());

        //Ensuite de la roue qui tourne
        this.getChildren().add(spinningWheel);
        spinningWheel.setTranslateY(7);
        spinningWheel.setTranslateX(Box.NODE_WIDTH - 50);
        spinningWheel.setMaxHeight(45);
        spinningWheel.getStylesheets().add(ProcessBox.class.getResource("ProgressIndicator.css").toExternalForm());

        //Et enfin de la progress bar
        this.getChildren().add(progressBar);
        progressBar.setTranslateY(40);
        progressBar.setTranslateX(5);
        progressBar.setPrefWidth(Box.NODE_WIDTH - 10);
        progressBar.getStylesheets().add(ProcessBox.class.getResource("ProgressBar.css").toExternalForm());

        this.getChildren().add(scrollPane);
        scrollPane.setTranslateX(5);
        scrollPane.setTranslateY(50);
        scrollPane.setPrefSize(Box.NODE_WIDTH-10, Box.NODE_HEIGHT-55);

        gridPane.setPrefWidth(Box.NODE_WIDTH-40);
        gridPane.setTranslateX(5);
        gridPane.setTranslateY(5);
        gridPane.setVgap(2);
        gridPane.setHgap(5);

        for (int i = 0; i < process.propertyManager().getNbProperties(); i++) {
            //On ajoute le nom de la propriété à la colonne 0 ligne i
            gridPane.add(new Text(process.propertyManager().getProperty(i).getName()), 0, i);
            //Et le node correspondant à la colonne 1 ligne i
            gridPane.add(process.propertyManager().getProperty(i).createEditNode(), 1, i);
        }
    }

    /**
     * Renvoie un objet Process.
     * @return Le process associé à la box.
     */
    public Process<?> getProcess() {
        return process;
    }


    @Override
    public void processEvent(ProcessEvent event) {
        switch (event.getType()) {
            case PAUSED_STATE_CHANGED:
                pauseState = true;
                break;
            case WAITING_STATE:
                waitingState = true;
                break;
            case PROGRESS_CHANGED:
                progressState = true;
                break;
            case NAME_CHANGED:
                nameState = true;
                break;
            case INTERRUPT_AFTER:
                interruptState = true;
                break;
        }
    }

    /**
     * Change l'état de la roue d'indication de travail en cours.
     * @param b true si la roue doit tourner, false sinon
     */
    private void setSpinState(boolean b) {
        if (b) {
            spinningWheel.setProgress(0);
            spinningWheel.setMaxHeight(45);
        }
        else {
            spinningWheel.setProgress(-1);
            spinningWheel.setMaxHeight(25);
        }
    }

    private void setProgress(double d) {
        progressBar.setProgress(d);
    }

    private void setName(String s) {this.getTitre().setText(s);}

    private void setButton(boolean b) {
        if (b) {
            startStopButton.setText(playChar);
        } else {
            startStopButton.setText(pauseChar);
        }
    }

    /**
     * Met à jour les différents états en fonction des événements qui ont déjà eu lieu.
     * Permet de différer la mise à jour et de ne pas la faire à chaque nouvel événement.
     */
    public void update() {
        if (pauseState) {
            pauseState = false;
            setButton(process.isPaused() || !process.isRunning());
        }
        if (waitingState) {
            waitingState = false;
            setSpinState(process.isWaiting());
        }
        if (progressState) {
            progressState = false;
            setProgress(process.getProgress());
        }
        if (nameState) {
            nameState = false;
            setName(process.getName());
        }
        if (interruptState) {
            interruptState = false;
        }
    }
}
