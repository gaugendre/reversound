/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package generictools;

/**
 * Created by gaby on 21/03/14.
 */
public class Pair <T1,T2> {

	private T1 v1;
	private T2 v2;

	/**
	 * Constuit en objet pair qui contient deux variable
	 * @param first Première valeur
	 * @param second Seconde Valeur
	 */
	public Pair(T1 first, T2 second) {
		v1 = first;
		v2 = second;
	}

    public Pair() {
    }

	/**
	 * Renvoie la première valeur de la paire
	 * @return La premiére valeur
	 */
	public T1 first() {
		return v1;
	}

	/**
	 * Modifie la première valeur
	 * @param first La nouvelle valeur
	 */
	public void setFirst(T1 first) {
		this.v1 = first;
	}


	/**
	 * Renvoie la seconde valeur de la paire
	 * @return La seconde valeur
	 */
	public T2 second() {
		return v2;
	}

	/**
	 * Modifie la seconde valeur
	 * @param second La seconde valeur
	 */
	public void setSecond(T2 second) {
		this.v2 = second;
	}
}