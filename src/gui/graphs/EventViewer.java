/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.graphs;

import conteneurs.NoteEvent;
import conteneurs.NoteEventList;
import conteneurs.NoteGamme;
import conteneurs.Profil;
import generictools.Instant;
import javafx.scene.paint.Color;
import processing.buffer.TemporalBuffer;

/**
 * Created by gaby on 31/05/14.
 */
public class EventViewer extends BargramView<TemporalBuffer<NoteEventList>> {
	OverlayGraph overlay;
	public EventViewer() {
		super("Evenorama", NoteEventList.class);

	}

	@Override
	protected Color getColor(int idFreq, Instant time) {
		NoteEventList list = buffer().get(time);
		if(list == null)
			return  null;

		NoteEvent event = null;
		if(getGamme()== NoteGamme.gamme())
			event = list.getByNoteId(idFreq);

		if(event==null)
			return null;

		float saturation = (float)Math.min(.2f + event.getEventAmpli()/1000000f, .92);
		return Color.hsb(event.getEventType()==NoteEvent.Type.APPARITION?85:13, saturation, .73);
	}

	@Override
	protected Runnable getOnMouseMoved(int idFreq, Instant time) {
		NoteEventList list = buffer().get(time);
		Runnable hide = new Runnable() {
			@Override
			public void run() {
				overlay.setVisibility(false);
			}
		};
		if(list == null)
			return hide;

		if(getGamme()!= NoteGamme.gamme())
			return hide;

		final NoteEvent event = list.getByNoteId(idFreq);

		if(event == null)
			return hide;

		return new Runnable() {
			@Override
			public void run() {
				((AbstractObservableGraph)overlay.getGraph()).setToShow(event.getNote().getProfil());
				overlay.setVisibility(true);
			}
		};
	}

	@Override
	protected AbstractBufferGraph getGenericGraph() {
		return new EventViewer();
	}

	@Override
	protected void initView() {
		super.initView();
		overlay = getGraphicView().createOverlay(getGraphicView().createGraphs(new Profil()).get(0), OverlayGraph.AutoPosition.MOUSE, false);
	}
}
