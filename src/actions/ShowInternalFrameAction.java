/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.package actions;
*/
package actions;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
/**
 * Created by Gabriel on 28/03/2014.
 */
public class ShowInternalFrameAction extends AbstractAction {
	private final JDesktopPane desktopPane;
	private final JInternalFrame internalFrame;

	public ShowInternalFrameAction(JDesktopPane desktopPane, JInternalFrame internalFrame) {
		super(internalFrame.getTitle());
		this.desktopPane = desktopPane;
		this.internalFrame = internalFrame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (!internalFrame.isVisible()) {
			desktopPane.add(internalFrame);
			internalFrame.setVisible(true);
		} else {
			try {
				internalFrame.setSelected(true);
			} catch (PropertyVetoException e1) {
				e1.printStackTrace(System.err);
			}
		}
	}
}