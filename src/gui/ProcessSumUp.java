/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package gui;

import gui.graphs.GraphicView;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import processing.AudioInputToBuffer;
import processing.ProcessControl;
import processing.processes.Process;

import java.util.ArrayList;

/**
 * Stocke les inputs et les process pour les afficher dans une fenêtre et permettre leur contrôle.
 */
public class ProcessSumUp extends GraphicView {

    //TODO: Contrôler les propriétés des traitements (right clic)

	private final MainWindow mW;

	protected static final int NODE_WIDTH = 300;
	protected static final int NODE_HEIGHT = 200;
    protected static final int TEXT_LENGTH = NODE_WIDTH/10-9;

    private final StackPane group = new StackPane();
    private final ScrollPane scrollPane = new ScrollPane(group);
    private final GridPane grid = new GridPane();
	private final Text frameRateLabel = new Text(10,10,"");

    private final ArrayList<Box> boxList = new ArrayList<>();


    /**
     * Constructeur.
     * @param mainWindow L'instance de fenêtre principale du programme.
     */
    public ProcessSumUp(MainWindow mainWindow) {
        super("Fenêtre de contrôle");
		initView();
		ProcessControl.instance().setProcessesView(this);
		mW = mainWindow;
    }

    /**
     * Initialisation de la vue.
     */
	public void initView() {
		Platform.runLater(() -> {
			setScene(new Scene(scrollPane, Color.WHITE));
			frameRateLabel.setFont(new Font("Candara", 5));
			frameRateLabel.setFill(Color.AZURE);
		});
	}

    /**
     * Mise à jour de la vue.
     */
	public void updateView(){
		Platform.runLater(this::generateGraph);
	}

    private void generateGraph() {
        grid.getChildren().clear();
        group.getChildren().clear();
        group.getChildren().add(grid);


        for (int i = 0; i < ProcessControl.instance().getInputNbr(); i++) {
            final AudioInputToBuffer input = ProcessControl.instance().getInput(i);
            final InputBox inputNode = createInputNode(input);

            emptyBoxList();

            setSuccessors(inputNode);
            inputNode.generateSuccCoord(0, i, boxList);
            for (int j = 0; j < boxList.size(); j++) {
                Box b = boxList.get(j);
                grid.add(b, b.getCoordinates().first(), b.getCoordinates().second());
                /*if (j < boxList.size()-1) {
                    group.getChildren().addNote(new Connector(b, boxList.get(j+1)));
                    group.getChildren().addNote(new Line(Box.NODE_WIDTH, Box.NODE_HEIGHT/2, Box.NODE_WIDTH+20, Box.NODE_WIDTH/2));
                }*/
            }
        }


		group.getChildren().add(frameRateLabel);

    }

    /**
     * Mise à jour du contenu des noeuds du panneau de contrôle.
     * @param frameRate Le taux de rafraîchissement.
     */
	public void update(Float frameRate){
		Platform.runLater(() -> {
			frameRateLabel.setText(frameRate.toString() + " fps");
            boxList.stream().filter(b -> b instanceof ProcessBox).forEach(b -> {
                ProcessBox pb = (ProcessBox) b;
                pb.update();
            });
		});
	}

    private void emptyBoxList() {
        boxList.stream().filter(b -> b instanceof ProcessBox).forEach(b -> {
            ProcessBox pb = (ProcessBox) b;
            pb.getProcess().setBoxDisplayed(false);
        });
        boxList.clear();
    }

    private void setSuccessors(InputBox box) {
        for (Process p : box.getInput().getBuffer().getUsedBy()) {
            setSuccessors(p, box);
        }
    }
    private void setSuccessors(ProcessBox box) {
        for (Process p : box.getProcess().getOutput().getUsedBy()) {
            setSuccessors(p, box);
        }
    }

    private void setSuccessors(Process p, Box box) {
        ProcessBox processBox = createProcessNode(p);
        box.addSuccessor(processBox);
        setSuccessors(processBox);
    }

	private InputBox createInputNode(final AudioInputToBuffer input){
        InputBox g = new InputBox(input);

        for (Node n : g.getClickableElements()) {
            n.addEventFilter(MouseEvent.MOUSE_CLICKED,
                    e -> {
                        mW.displayBuffer(ProcessControl.instance().getBufferIndex(input.getBuffer()));
                        e.consume();
                    }
            );
        }

		return g;
	}

	private ProcessBox createProcessNode(final Process process){
        ProcessBox g = new ProcessBox(process);

        for (Node n : g.getClickableElements()) {
            n.addEventFilter(MouseEvent.MOUSE_CLICKED,
                    e -> {
                        mW.displayBuffer(ProcessControl.instance().getBufferIndex(process.getOutput()));
                        e.consume();
                    }
            );
        }

		return g;
	}
}
