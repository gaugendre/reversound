/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.buffer;

import java.util.ArrayList;


public interface Storage<T> {

	public enum Type{
		ArrayList
	}

	/**
	 * Retourne l'élément d'index id, null si aucun n'y est associé
	 * @param id index de l'élément
	 * @return l'élément    /!\ Peut renvoyer null si aucun élément ne correspond à l'index /!\
	 */
	public T get(int id);

	/**
	 * Modifie un élément à un index donné ou l'ajoute si aucun élément ne correspond à l'index
	 * @param id index de l'élément à modifier ou ajouter
	 * @param value valeur de l'élément
	 * @return la valeur de l'élément précédant  /!\ Peut renvoyer null si aucun élément ne correspondait à cet index /!\
	 */
	public T set(int id, T value);

	/**
	 * Ajoute une élément en fin de liste
	 * @param value valeur de l'élément à ajouter
	 * @return Vrai si l'opération a réussie (mais peut-elle rater?)...
	 */
	public boolean add(T value);

	/**
	 * Supprime un élément de la liste
	 * @param id index de l'élément à supprimer
	 * @return la valeur de l'élément supprimer /!\ Peut renvoyer null si aucun élément ne correspondait à cet index /!\
	 */
	public T remove(int id);

	/**
	 * Retourne le nombre maximum d'élément de tableau (dernier index +1)
	 * @return la taille de l'espace de stockage
	 */
	public int size();

	/**
	 * Vérifie si tout les échantillons du stockage sont null
	 * @return Vrai si size() = 0 ou si aucun échantillon n'est stocké dans l'espace disponible
	 */
	public boolean isEmpty();

	/**
	 * Retourne la liste des indexs associés à des éléments dans le Storage: aucun de ces indexs ne retournera null par get(index)
	 * * @return la liste des indexs de tout les éléments du Storage
	 */
	public ArrayList<Integer> getSamplesIndex();

	/**
	 * Génère un iterateur pointant initialement sur le première élément de la liste
	 * @return Un iterateur sur ce stockage
	 */
	public StorageIterator<T> getIterator();

	/**
	 * Génère un iterateur pointant initialement sur l'élément de la liste correspondant à l'index en argument
	 * @param initialIndex index de l'élément sur lequel pointe initialement l'iterateur
	 * @return Un iterateur sur ce stockage
	 */
	public StorageIterator<T> getIterator(int initialIndex);

	/**
	 * Réserve directement un espace sans modifier la taille du tableau (si la forme si prête)
	 * @param capacity capacité maximale;
	 */
	public void tryEnsureCapacity(int capacity);
}
