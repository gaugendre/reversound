/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.graphs;

import conteneurs.ObservableObject;
import conteneurs.Spectre;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.text.Font;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

/**
 * Created by gaby on 01/06/14.
 */
public abstract class AmpliFreqView<T extends ObservableObject> extends AbstractObservableGraph<T>{

	protected BarChart<String, Number> currentBarChart;
	private ArrayList<String> categories = new ArrayList<>();
	private Boolean ampliInDb = false;

	public enum Type{
		REGULAR_FREQ,
		NOTES
	}

	AmpliFreqView(String name, Class type) {
		super( name, type);
	}

	private void createContent() {

		// Définition des axes du graphe
		CategoryAxis xAxis = new CategoryAxis();
		NumberAxis yAxis = new NumberAxis(0, 1, 1);
		currentBarChart = new BarChart<>(xAxis,yAxis );


		createXAxis();


		// Définition de certaines propriétés graphiques du graphe
		currentBarChart.getStylesheets().add(SpectreView.class.getResource("AudioBarChart.css").toExternalForm());
		currentBarChart.setLegendVisible(false);
		currentBarChart.setAnimated(false);
		currentBarChart.setBarGap(0);
		currentBarChart.setCategoryGap(0);
		currentBarChart.setVerticalGridLinesVisible(false);


		// Initialisation du graphe
		xAxis.setTickLabelFont(new Font(10));
		yAxis.setLabel("Amplitude");
		yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(yAxis, null, ""));
	}

	/**
	 * Doit renvoyer le noeud JavaFX élémentaire du graph
	 *
	 * @return Le noeud parent
	 */
	@Override
	public Parent getMainNode() {
		return currentBarChart;
	}

	/**
	 * Revoie le liste des éléments du menu contextuel
	 *
	 * @param contextMenu
	 */
	@Override
	public void createContextMenu(JPopupMenu contextMenu) {
		JMenu choixDB = new JMenu("Axe amplitude");
		JRadioButtonMenuItem linAmpli = new JRadioButtonMenuItem("Amplitude linéaire",!ampliInDb);
		linAmpli.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED) {
					ampliInDb = false;
					state().invalidate();
				}
			}
		});
		JRadioButtonMenuItem dBAmpli = new JRadioButtonMenuItem("Amplitude en déciBel",ampliInDb);
		dBAmpli.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED){
					ampliInDb = true;
					((NumberAxis)currentBarChart.getYAxis()).setUpperBound(1);
					state().invalidate();
				}
			}
		});
		ButtonGroup b2 = new ButtonGroup();
		b2.add(linAmpli);
		choixDB.add(linAmpli);
		b2.add(dBAmpli);
		choixDB.add(dBAmpli);

		contextMenu.add(choixDB);
	}

	@Override
	public void updateViewer() {
		Platform.runLater(
				new Runnable() {
					@Override
					public void run() {

						currentBarChart.setTitle("");
						if (toShow() == null) {
							cleanView();
							return;
						}
						//s = SpectreFilterFactory.hearingCorrection().setIntensity(0).filterSpectre(s);
						float yMax = 0f;


						for (int i = 0; i < categories.size(); i++) {

							float ampli = getAmplitude(i);

							if(ampliInDb)
								ampli = Spectre.ampliTodB(ampli);

							currentBarChart.getData().get(0).getData().get(i).setYValue(ampli);
							if (ampli > yMax) {
								yMax = ampli;
							}
						}

						if (yMax > Float.MAX_VALUE) {
							cleanView();
							currentBarChart.setTitle("Spectre incorrect");
							return;
						}

						NumberAxis yAxis = ((NumberAxis) currentBarChart.getYAxis());
						// Changement du max d'axe Y si le nouveau max est plus grand que l'ancien.
						yMax = yMax > yAxis.getUpperBound() ? yMax * 1.01f : (float) yAxis.getUpperBound() * 0.995f;

						yAxis.setTickUnit(Math.pow(10, (int) (Math.log10(yMax) - .2)));
						yAxis.setUpperBound(yMax);
					}
				}
		);
	}


	@Override
	public void cleanView() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				currentBarChart.setTitle("Spectre indisponible");
			}
		});

	}

	public void setXAxisCategories(ArrayList<String> categories){
		this.categories = categories;
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				createXAxis();
				state().invalidate();
			}
		});
	}

	private void createXAxis(){

		if(currentBarChart.getData().isEmpty())
			currentBarChart.getData().add(new XYChart.Series<>());
		else
			currentBarChart.getData().get(0).getData().clear();

		for (int i = 0; i < categories.size(); i++)
			currentBarChart.getData().get(0).getData().add(new XYChart.Data<>(categories.get(i), 0));
	}

	protected abstract float getAmplitude(int idCategory);


	protected void initView() {
		createContent();
	}
}
