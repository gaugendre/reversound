/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

import java.util.ArrayList;

/**
 * Associe à un numéro d'harmonique, une amplitude
 */
public class Profil extends ObservableObject{
	private ArrayList<Float> amplitudes = new ArrayList<>();

	public Profil() {

	}

	/** Définit/Modifie l'amplitude d'une fréquence à partir de son indice */
    public void setAmplitude (float newAmplitude, int idHarmonique) {

		if(newAmplitude==0 && idHarmonique == amplitudes.size()-1) {
			amplitudes.remove(idHarmonique);
			return;
		}

		ensureSize(idHarmonique+1);
		this.amplitudes.set(idHarmonique, newAmplitude);
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
	}

	/** Renvoie l'amplitude de l'harmonique à l'indice considéré */
    public float getAmplitude (int idHarmonique) {
        return idHarmonique<amplitudes.size()?this.amplitudes.get(idHarmonique):0f;
    }

	/** Renvoie le nombre d'harmoniques */
	public int size(){
		return amplitudes.size();
	}

	/** Renvoie les identifiants des harmoniques non nulles */
	public ArrayList<Integer> getHarmoniqueID(){
		ArrayList<Integer> r = new ArrayList<>(amplitudes.size());
		for (int i = 0; i < amplitudes.size(); i++)
			if (amplitudes.get(i) != 0)
				r.add(i);

		r.trimToSize();
		return r;
	}

	/** Vérifie le nombre d'harmoniques > la taille donnee en parametre
	 * @param size la taille en entier
	 * @return boolean vrai si size<au nombre d'harmoniques, faux sinon.	 */
	private boolean ensureSize(int size){
		if(size<=amplitudes.size())
			return true;

		amplitudes.ensureCapacity(size);

		while(amplitudes.size()<size)
			amplitudes.add(0f);

		return false;
	}

	@Override
	public String toString() {
		return "Profil";
	}

	@Override
	public Class getType() {
		return Profil.class;
	}
}
