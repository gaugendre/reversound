/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

/**
 * Created by gaby on 06/05/14.
 *
 * Evenement d(objet observable
 */
public class ObservableObjectEvent {
	ObservableObject  emmiter;
	Type type;

	/**
	 * Constructeur de l'évenement
	 * @param emmiter Objet écoutable source de l'évenement
	 * @param type type de l'évenement
	 */
	public ObservableObjectEvent(ObservableObject emmiter, Type type) {
		this.emmiter = emmiter;
		this.type = type;
	}

	/**
	 * Retourne l'objet écoutable à l'origine de l'évenement
	 * @return emmeteur
	 */
	public ObservableObject getEmmiter() {
		return emmiter;
	}


	/**
	 * @return type de l'évenement
	 */
	public Type getType() {
		return type;
	}

	public String toString(){
		String r = emmiter.toString() + " Event: ";
		switch (type){
			case DATA_CHANGED:
				r+="Modified";
				break;
		}
		return r;
	}

	/**
	 * Type d'évenement:
	 * 		DATA_CHANGED: les données ont été modifiés
	 * 		ID_CHANGED: les identifiants de localisation de l'objet ont été modifiées (propre aux Note)
	 */
	public enum Type {
		DATA_CHANGED,
		ID_CHANGED
	}
}
