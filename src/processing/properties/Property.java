/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.properties;

import javafx.application.Platform;
import javafx.scene.control.Control;

import java.util.ArrayList;

/**
 * Created by gaby on 03/04/14.
 */
public abstract class Property <T> {
	private final String name;
	private T value;

	private boolean locked = false;
	private T temporaryValue;

	private final Class type;

	private final ArrayList<Listener<T>> listeners;

	public Property(String name, Class type, T value) {
		this.name = name;
		this.type = type;
		this.value = value;

		listeners = new ArrayList<>();
	}

	/**
	 * Récupère la valeur de la propriété
	 * @return le valeur de la propriété
	 */
	public final T getValue() {
		return value;
	}

	public final T getApparentValue(){
		if(locked)
			return temporaryValue;
		return value;
	}

	/**
	 * Modifie la valeur de la propriété
	 * @param value la nouvelle valeur
	 * @return Renvoie vrai si l'opération s'est bien déroulé (mais peut-elle mal se passer?)
	 */
	public boolean setValue(final T value) {
		nodeValueModified(value);
		return true;
	}

	/**
	 * Méthode à appeler pour gerer la modification de la valeur de la propriété en interne
	 * @param value la nouvelle valeur
	 */
	protected final void nodeValueModified(final T value){
		if(locked)
			temporaryValue = value;
		else
			changeRealValue(value);

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				syncNodesValue();
			}
		});
	}

	private final void changeRealValue(final T value){
		T previousValue = this.value;
		this.value = value;
		emitChangeEvent(previousValue);
	}


	//--------------------------  Manage Event Listener -----------------------

	private void emitChangeEvent(T previousValue){
		for(Listener<T> p: listeners)
			p.propertyChange(this,previousValue);
	}

	/**
	 * Ajoute un listener à cette propriété, lorsquelle sera modifié, le listener sera prévenu
	 * @param listener le listener à ajouter
	 * @return Vrai si l'opération d'ajout s'est bien déroulé (si le listener n'avais pasété déjà ajouté)
	 */
	public final boolean addListener(Listener<T> listener){
		if(listeners.contains(listener))
			return false;

		listeners.add(listener);
		return true;
	}

	/**
	 * Supprime le listener de la liste des listeners liés à cette propriété
	 * @param listener le listener à supprimer
	 * @return Renvoie vrai si le listener existait dans la liste et à bien été supprimer
	 */
	public final boolean removeListener(Listener<T> listener){
		if(!listeners.contains(listener))
			return false;

		listeners.remove(listener);
		return true;
	}


	public interface Listener<T> {
		public void propertyChange(Property source, T previousValue);
	}


	//--------------------------   Manage Type  -------------------------------

	/**
	 * Retourne le type de la propriété (equivalent  T)
	 * @return Le type de la proprriété
	 */
	public final Class getType() {
		return type;
	}


	/**
	 * @return le nom de la propriété
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Retourne l'élément javaFX de modification de la propriété
	 * @return L'élément graphique lié à la propriété
	 */
	public abstract Control createEditNode();

	/**
	 * Appeler lorsque la proriété est modifier par autre chose que le l'élément graphique,
	 * il faut donc mettre à jour se dernier
	 */
	protected abstract void syncNodesValue();


	public void lock(){
		if(locked)
			unlock();
		temporaryValue = value;
		locked = true;
	}
	public boolean unlock(){
		if(temporaryValue==value)
			return false;

		changeRealValue(temporaryValue);
		temporaryValue = null;
		locked = false;
		return true;
	}

	public boolean isLocked() {
		return locked;
	}
}
