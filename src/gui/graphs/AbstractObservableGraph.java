/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.graphs;

import conteneurs.ObservableObject;
import gui.PlayerControl;
import gui.PlayerControlEvent;
import gui.viewer_state.BufferViewerState;
import gui.viewer_state.ObservableViewerState;
import gui.viewer_state.Viewer;
import gui.viewer_state.ViewerState;
import processing.buffer.Buffer;
import processing.buffer.TemporalBuffer;

/**
 * Created by gaby on 09/05/14.
 * Classe abstraite définissant les attributs et méthode communes au graph d'objet observable
 */
public abstract class AbstractObservableGraph<T extends ObservableObject> extends AbstractGraph implements Viewer, PlayerControl.Listener {

	private T toShow=null;
	private Buffer<T> buffer = null;
	private int currentID = -1;
	private ViewerState state;

	protected AbstractObservableGraph(String name, Class type) {
		super(name, type);
	}

	@Override
	public AbstractGraph setupGraph(Buffer buffer, GraphicView graphicView, boolean syncWithPlayer) {
		/*Class[] classes = b.getType().getClasses();
		for (int i = 0; i < classes.length; i++) {
			if(classes[i] == ObservableObject.class)
				break;
			else if(i == classes.length-1)
				return null;
		}*/

		AbstractObservableGraph g = getGenericGraph();
		g.setGraphicView(graphicView);
		g.setBuffer(buffer);


		if(syncWithPlayer) {
			/*if(buffer instanceof TemporalBuffer)
				PlayerControl.instance().addListener(g);
			else
				return null;
			/*/
			PlayerControl.instance().addListener(g);
			//*/
		}


		g.setupView();


		g.setState(BufferViewerState.singleFrameWindow(g,buffer));
		return g;
	}

	@Override
	public AbstractGraph setupGraph(ObservableObject o, GraphicView graphicView) {
		AbstractObservableGraph g = getGenericGraph();
		g.setGraphicView(graphicView);
		g.setState(new ObservableViewerState(g,o));
		g.setToShow(o);

		g.setupView();
		return g;
	}

	@Override
	public boolean isValid() {
		return buffer!=null||toShow!=null;
	}

	@Override
	protected void visibilityChange(boolean isVisible) {
		state.setVisibility(isVisible);
	}

	@Override
	public void playerControlEvent(PlayerControlEvent e) {
		switch (e.getType()){
			case FRAME:
				int id = e.getFrame().mapToIndex((TemporalBuffer)buffer);
				currentID = id;
				((BufferViewerState)state).setSingleFrame(id);
				break;
		}
	}

	@Override
	public void updateView() {
		if(buffer != null) {
			setToShow(buffer.get(currentID));
		}

		if(toShow!=null)
			updateViewer();
	}

	protected abstract AbstractObservableGraph getGenericGraph();

	public T toShow() {
		return toShow;
	}

	public void setToShow(T toShow) {
		this.toShow = toShow;

		if(state instanceof ObservableViewerState)
			((ObservableViewerState)state).setObservableObject(toShow);
	}

	public Buffer<T> buffer() {
		return buffer;
	}

	private void setBuffer(Buffer<T> buffer) {
		this.buffer = buffer;
	}


	public ViewerState state() {
		return state;
	}

	private void setState(ViewerState state) {
		this.state = state;
	}

	protected abstract void updateViewer();
}
