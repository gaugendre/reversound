/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package processing;

import conteneurs.Spectre;

/**
 * Created by gaby on 29/04/14.
 */
public class SpectreFilterFactory {
	static public SpectreFilter hearingCorrection(){
		return new SpectreFilter() {
			@Override
			protected void init() {
				autoIntensity = false;
			}

			@Override
			protected float filter(int idFreq, Spectre spectre) {
				float f = spectre.getGamme().getFreq(idFreq)/1000f;
				float e = f-3.3f;
				return Spectre.dBToAmpli(spectre.getdBAmplitude(idFreq)-filterIntensity*(3.64f*(float)Math.pow(f,-.08f)-6.5f*(float)Math.exp(-.06*e*e)+(float)Math.pow(10f,-3f*f*f*f*f)));
			}
		};
	}

}
