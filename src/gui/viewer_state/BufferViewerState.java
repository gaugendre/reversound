/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.viewer_state;

import processing.buffer.Buffer;
import processing.buffer.BufferEvent;
import processing.buffer.BufferEventListener;

/**
 * Created by gaby on 09/04/14.
 *
 * Classe controlleur s'interposant entre les buffers et leur visualisateur,
 * il gère les mises à jour différée dans le temps et les modifications de fenêtres du visualisateur
 */
public class BufferViewerState extends ViewerState implements BufferEventListener{

	public enum Type{
		SINGLE_FRAME,
		CONTINUOUS_WINDOW,
		ALL_DATA,
		NO_DATA
    }

	final private Type type;
	final private boolean bufferSizeSensible;

	private int idMin=0, idMax=0;
	private Buffer buffer;


	private BufferViewerState(Buffer buffer, Viewer viewer, Type type, boolean bufferSizeSensible){
		super(viewer);
		this.type = type;
		this.bufferSizeSensible = bufferSizeSensible;
		this.buffer = buffer;
		buffer.addBufferListener(this);
		init();
	}

	static public BufferViewerState singleFrameWindow( Viewer viewer, Buffer buffer){
		return new BufferViewerState(buffer, viewer, Type.SINGLE_FRAME, false);
	}
	static public BufferViewerState continousWindow(Viewer viewer, Buffer buffer){
		return new BufferViewerState(buffer, viewer, Type.CONTINUOUS_WINDOW, false);
	}
	static public BufferViewerState allDataWindow(Viewer viewer, Buffer buffer){
		return new BufferViewerState(buffer, viewer, Type.ALL_DATA, true);
	}
	static public BufferViewerState sizeSensible(Viewer viewer, Buffer buffer){
		return new BufferViewerState(buffer, viewer, Type.NO_DATA, true);
	}

	public void setWindow(int idMin, int idMax){
		this.idMax = idMax;
		this.idMin = idMin;

		invalidate();
	}
	public void setSingleFrame(int id){
		idMax = id;
		idMin = id;

		invalidate();
	}



	public Type getType(){
		return type;
	}

	public int getMaximumIndex(){
		if(type==Type.CONTINUOUS_WINDOW)
			return idMax;

		return -1;
	}

	public int getMinimumIndex(){
		if(type==Type.CONTINUOUS_WINDOW)
			return idMin;

		return -1;
	}

	public int getSingleIndex(){
		if(type == Type.SINGLE_FRAME)
			return idMin;

		return -1;
	}

	public boolean windowContains(int sampleID){
		switch (type){
			case NO_DATA:
				return false;
			case ALL_DATA:
				return true;
			case CONTINUOUS_WINDOW:
				return sampleID >= idMin && sampleID <= idMax;
			case SINGLE_FRAME:
				return sampleID == idMin;
		}
		return false;
	}

	@Override
	public void bufferEvent(BufferEvent e) {
		if (e.getType() == BufferEvent.Type.SAMPLE_ADDED || e.getType() == BufferEvent.Type.SAMPLE_CHANGED){
			if (windowContains(e.getSampleID()))
				setState(State.UPDATE_NEEDED, slowingFactorOnDataChange());
		}else if(e.getType()== BufferEvent.Type.SAMPLE_DELETED){
				if(type==Type.SINGLE_FRAME)
					if(e.getSampleID()==idMin)

						setState(State.CLEAN_NEEDED, slowingFactorOnDataChange());
				else if(windowContains(e.getSampleID()))
						setState(State.UPDATE_NEEDED, slowingFactorOnDataChange());

		}else if(e.getType()== BufferEvent.Type.INVALIDATE)
				invalidate();

		if(bufferSizeSensible&&(e.getType()== BufferEvent.Type.SAMPLE_ADDED||e.getType()== BufferEvent.Type.SAMPLE_DELETED))
			setState(State.UPDATE_NEEDED, slowingFactorOnDataChange());

	}

	@Override
	protected boolean canDraw() {
		switch(type){
			case CONTINUOUS_WINDOW:
				return idMin< buffer.size();
			case SINGLE_FRAME:
				return buffer.get(idMin) != null;
		}
		return !buffer.isEmpty();
	}

	@Override
	protected void visibilityChanged() {
		if(visibility)
			buffer.addBufferListener(this);
		else
			buffer.removeBufferListener(this);
	}

	public Buffer getBuffer() {
		return buffer;
	}

	public boolean isSizeSensitive(){
		return bufferSizeSensible;
	}
}
