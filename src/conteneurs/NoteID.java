/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

import processing.buffer.NoteBuffer;

/** Created by gaby on 02/06/14.
 * Contient l'identifiant d'une note. Une note jouée est déclarée à tous les instants où elle est jouée,
 * mais l'identifiant de la note est commun car il s'agit de la meme note. Fait le lien entre tous les instants
 * ou une note est jouee */
public class NoteID extends ObservableObject implements ObservableObject.Listener{
    private NoteBuffer buffer;
	private int bufferID=-1;

	public NoteID(Note note){
		bufferID = note.getBufferID();
		buffer = note.getBuffer();
		note.addListener(this);
	}

	@Override
	public Class getType() {
		return NoteID.class;
	}

	public int getBufferID() {
		return bufferID;
	}

	public NoteBuffer getBuffer() {
		return buffer;
	}

	public Note note(){
		if(buffer==null)
			return null;

		return buffer.getNote(bufferID);
	}

	public boolean isValid(){
		return buffer!=null && bufferID!=-1;
	}

	@Override
	public void observableOjectEvent(ObservableObjectEvent e) {
		emit(e.getType());
		if(e.getType()== ObservableObjectEvent.Type.ID_CHANGED){
			buffer = ((Note)e.getEmmiter()).getBuffer();
			bufferID = ((Note)e.getEmmiter()).getBufferID();
		}
	}
}
