/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

/**
 * Created by gaby on 02/06/14.
 */
public class NoteList extends ObservableList<NoteID> {


	public NoteList() {
	}


	/** Renvoie la note dans la liste de note dont on donne l'identifiant en paramètre */
	public Note getNoteById(int noteID) {
		int id = getIdById(noteID);
		if(id>-1)
			return get(id).note();
		return null;
	}

	/** Renvoie l'identifiant de la note à partir de son index (exemple : la 49) */
	public int getIdById(int noteID){
		for (int id = 0; id < size() && get(id).note().getNoteID() <= noteID; id++)
			if (get(id).note().getNoteID() == noteID)
				return id;

		return -1;
	}

	/** Permet d'enlever une note  à la liste de note */
	public boolean remove(Note note){
		int id = getIdById(note.getNoteID());

		if(id>-1){
			remove(id);
			return true;
		}
		return false;
	}

	/** Permet d'ajouter une note à la liste de note */
	public void add(Note note){
		add(new NoteID(note));
	}

	/** Permet d'ajouter une note à partir de son identifiant de note */
	public void add(NoteID noteID) {

		int addId = 0;
		float freq = noteID.note().getFreq();
		for(addId=size()-1; addId>=0; addId--){
			if(get(addId).note().getFreq() < freq){
				break;
			}else if(get(addId).note().getFreq() == freq){
				set(addId, noteID);
				return;
			}
		}
		list.add(addId+1, noteID);
	}

}
