/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.graphs;

import conteneurs.Spectre;
import generictools.Instant;
import javafx.scene.paint.Color;
import processing.buffer.TemporalBuffer;

/**
 * Created by gaby on 31/05/14.
 */
public class Spectrogram extends BargramView<TemporalBuffer<Spectre>> {

	Instant optiCastTime = new Instant(-1);
	Spectre optiSpectre = null;

	private final float MAX = 4000000;

	public Spectrogram() {
		super("Spectro", Spectre.class);

	}

	@Override
	protected Color getColor(int idFreq, Instant time) {
		if(!time.equals(optiCastTime)) {
			optiCastTime = time;
			optiSpectre = buffer().get(time);
			if(optiSpectre!=null) {
				optiSpectre = optiSpectre.copy();
				if (optiSpectre.getGamme() != getGamme())
					optiSpectre.castToGamme(getGamme());
			}
		}

		if(optiSpectre == null)
			return null;

		return colorRamp(optiSpectre.getAmplitude(idFreq), MAX);
	}

	public Color colorRamp(float pos, float maxPos){

		if(pos == 0 || maxPos == 0)
			return Color.hsb(0,0,1);

		float h1 = 360;
		float h2 = 330;
		float h3 = 310;
		if(pos<0){
			h1 = 190;
			h2 = 200;
			h3 = 222;
			pos = -pos;
		}

		pos = Math.min(pos, maxPos-1);

		float x = pos/maxPos;
		x = x*x;

		if(x <= .08) {
			x/=.08;
			return Color.hsb(h1, x*.65f, 1);
		}

		x-=.08;
		if(x <= .52) {
			x/=.52;
			return Color.hsb(((1f-x) * h1) + (x * h2), ((1f-x) * .65f) + (x * .8f), (1f-x) + (x * .75f));
		}
		x-=.52;
		x/=.4;

		if(((1f-x) * h2) + (x * h3)>222 && ((1f-x) * h2) + (x * h3)<310)
			System.out.println("x:"+x+" h2:"+h2+"/ h3:"+h3);

		try {
			return Color.hsb(((1f-x) * h2) + (x * h3), ((1f-x) * .8f) + (x * 1f), ((1f-x) * .75f) + (x * .53f));
		}catch (Exception e){

			e.printStackTrace();
		}
		return Color.hsb(h3, 1f, .53f);
	}


	@Override
	protected AbstractBufferGraph getGenericGraph() {
		return new Spectrogram();
	}
}
