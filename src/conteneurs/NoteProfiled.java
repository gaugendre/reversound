/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

/**
 * Created by gaby on 14/05/14.
 */
public class NoteProfiled extends ObservableObject{
	private int idNote;
	private Profil profil;

	public NoteProfiled(int idNote, Profil profil) {
		this.idNote = idNote;
		this.profil = profil;
	}


	@Override
	public Class getType() {
		return NoteProfiled.class;
	}

	public Profil getProfil() {
		return profil;
	}

	/** Permet de definir le profil d'une note, cad l'amplitude des harmoniques
	 * @param profil le profil de la note */
	public void setProfil(Profil profil) {
		this.profil = profil;
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
	}

	/** Indice qui caracterise la hauteur de la note */
	public int getIdNote() {
		return idNote;
	}

	/** Renvoie la frequence d'une note à partir de son indice (exemple: la 49) */
	public float getFreq(){
		return NoteGamme.getStatFreq(idNote);
	}

	/** Renvoie le nom de la note a partir de sa hauteur */
	public String getNoteName(){
		return NoteGamme.getNoteName(idNote);
	}

	public String toString(){
		return getNoteName();
	}
}
