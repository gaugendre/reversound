/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui;

import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import processing.AudioInputToBuffer;

/**
 * Définit la Box d'une Input, simple rectangle gris avec le nom de l'input.
 */
public class InputBox extends Box {
    private final AudioInputToBuffer input;

    /**
     * Génère la box
     * @param input Le buffer d'input à associer.
     */
    public InputBox(AudioInputToBuffer input) {
        super(Color.WHITESMOKE, new Text(input.getName()));
        this.input = input;
    }

    /**
     * Permet de récupérer le buffer d'input associé à la box.
     * @return Le buffer d'input associé à la box.
     */
    public AudioInputToBuffer getInput() {
        return input;
    }
}
