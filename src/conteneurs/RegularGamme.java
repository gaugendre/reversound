/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

import java.util.ArrayList;

/**
 * Created by Gabriel on 26/03/2014.
 * Crée une gamme avec des fréquences régulièrement réparties (intervalle constant entre 2 fréquences
 */
public class RegularGamme extends Gamme{

	private final float startFreq;
    private final float step;
	private final int lengthGamme;

	public RegularGamme(float startFreq, int lengthGamme, float step) {
				this.startFreq = startFreq;
				this.lengthGamme = lengthGamme;
				this.step = step;
	}

	public RegularGamme(float startFreq, float endFreq, int nbrPoint){
		this(startFreq, nbrPoint, (endFreq-startFreq)/(float)nbrPoint);
	}

	/** Renvoie l'indice de la fréquence la plus proche si on tombe dessus,
	 * sinon renvoie une ArrayList contenant l'indice précedent, l'indice suivant et un coefficient pour situer la note entre les 2. */
	public ArrayList<Number> getIndex(float freq) {
		if(freq > this.getEndFreq()){
			ArrayList<Number> res = new ArrayList<>();
			res.add(-1);
			return res;
		} else if( (freq-this.startFreq)%this.step == 0 ){
			ArrayList<Number> res = new ArrayList<>();
			res.add(Math.round(freq-this.startFreq)/step);
			return res;
		} else {
			int indexPrec = (int)Math.floor((freq-this.startFreq)/this.step);
			float coeff = (freq - this.getFreq(indexPrec))/ this.step;
			ArrayList<Number> res = new ArrayList<>();
			res.add(indexPrec);
			res.add(indexPrec+1);
			res.add(coeff);
			return res;
		}

	}

	/** Renvoie la fréquence associée à un indice donne en paramètre */
	public float getFreq(int index) {
		if(index < this.lengthGamme) {
			return this.startFreq + index * this.step;
		}
		return -1;
	}

	/** Renvoie le nombre de fréquences contenues dans la gamme */
	public int gammeSize() {
		return this.lengthGamme;
	}

	/** Renvoie la fréquence de début: par défaut c'est 0 */
	public float getStartFreq() {
		return this.startFreq;
	}

	/** Renvoie la fréquence maximale de la gamme */
	float getEndFreq() {
		return (this.lengthGamme-1)*this.step + this.startFreq;
	}

	public float getStep() {
		return this.step;
	}
}
