/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.properties;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Control;

import java.util.ArrayList;

/**
 * Created by gaby on 27/05/14.
 */
public class BooleanProperty extends Property<Boolean> {
	ArrayList<CheckBox> nodes = new ArrayList<>();

	public BooleanProperty(String name, boolean value) {
		super(name, Boolean.class, value);
	}

	public BooleanProperty(String name){
		this(name, false);
	}

	/**
	 * Retourne l'élément javaFX de modification de la propriété
	 *
	 * @return L'élément graphique lié à la propriété
	 */
	@Override
	public Control createEditNode() {
		CheckBox node = new CheckBox();
		node.setOnAction(actionEvent -> {
            if(node.isSelected()!=getValue())
                nodeValueModified(node.isSelected());
        });
		node.setSelected(getValue());
		return node;
	}

	/**
	 * Appeler lorsque la proriété est modifier par autre chose que le l'élément graphique,
	 * il faut donc mettre à jour se dernier
	 */
	@Override
	protected void syncNodesValue() {
		for(CheckBox c: nodes)
			c.setSelected(getApparentValue());
	}
}
