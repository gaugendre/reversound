/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.graphs;

import conteneurs.Gamme;
import conteneurs.NoteGamme;
import conteneurs.RegularGamme;
import conteneurs.Spectre;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


/**
 * Created by Gabriel on 01/04/2014.
 */
public class SpectreView extends AmpliFreqView<Spectre>{

	private Gamme xAxisGamme;

	public enum Type{
		REGULAR_FREQ,
		NOTES
	}

	SpectreView() {
		super( "Visualisateur de spectre", Spectre.class);
	}

	@Override
	protected AbstractObservableGraph getGenericGraph() {
		return new SpectreView();
	}

	/**
	 * Revoie le liste des éléments du menu contextuel
	 *
	 * @param contextMenu
	 */
	@Override
	public void createContextMenu(JPopupMenu contextMenu) {
		super.createContextMenu(contextMenu);

		JMenu choixAxesX = new JMenu("Axe fréquence");
		JRadioButtonMenuItem noteGamme = new JRadioButtonMenuItem("Gamme chromatique",xAxisGamme instanceof NoteGamme);
		noteGamme.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED)
					setXAxisGamme(NoteGamme.gamme());
			}
		});
		JRadioButtonMenuItem regularGamme = new JRadioButtonMenuItem("Gamme linéaire",xAxisGamme instanceof RegularGamme);
		regularGamme.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==ItemEvent.SELECTED)
					setXAxisGamme(new RegularGamme((float)Math.pow(10, (int)Math.log10(NoteGamme.getMinFreq())),(float)Math.pow(10, (int)Math.log10(NoteGamme.getMaxFreq())),1000));
			}
		});
		ButtonGroup b = new ButtonGroup();
		b.add(noteGamme);
		choixAxesX.add(noteGamme);
		b.add(regularGamme);
		choixAxesX.add(regularGamme);

		contextMenu.add(choixAxesX);
	}

	@Override
	public void setToShow(Spectre toShow) {
		if(toShow != null) {
			toShow = toShow.copy();
			if(toShow.getGamme() != xAxisGamme)
				toShow.castToGamme(xAxisGamme);
		}
		super.setToShow(toShow);
	}

	public void setXAxisGamme(Gamme xAxisGamme){
		this.xAxisGamme = xAxisGamme;
		if(toShow()!=null)
			toShow().castToGamme(xAxisGamme);
		setXAxisCategories(xAxisGamme.toStringList());
	}

	protected void initView() {
		super.initView();
		setXAxisGamme(NoteGamme.gamme());
	}

	@Override
	protected float getAmplitude(int idCategory) {
		return toShow().getAmplitude(idCategory);
	}}
