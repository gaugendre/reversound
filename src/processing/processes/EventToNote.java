/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.processes;

import conteneurs.Note;
import conteneurs.NoteEvent;
import conteneurs.NoteEventList;
import conteneurs.NoteList;
import generictools.Instant;
import processing.buffer.NoteBuffer;
import processing.buffer.Storage;
import processing.buffer.TemporalBuffer;
import processing.properties.FloatProperty;

import java.util.ArrayList;

/**
 * Created by gaby on 03/06/14.
 */
public class EventToNote extends IndexedProcess<NoteList>{

	ArrayList<Note> stillAlive = new ArrayList<>();

	FloatProperty minDuration;

	public EventToNote( TemporalBuffer<NoteEventList> input) {
		super("Event to note", input, new NoteBuffer(Storage.Type.ArrayList, input.getSampleRate(), input.getOriginInstant()));
		minDuration = new FloatProperty("Durée minimum (ms)", 100, 0, 2000);
		propertyManager().addProperty(minDuration);
	}

	/**
	 * À  redéfinir: Retourne le résultat du calcul à placer à l'index outputID
	 *
	 * @param outputID index de l'échantillon que l'on veut calculer
	 * @return Résultat du calcul
	 */
	@Override
	protected NoteList compute(Instant outputID) {
		NoteEventList list = input().get(outputID);
		NoteList result = new NoteList();

		for (int i = 0; i < list.size(); i++) {
			NoteEvent e = list.get(i);

			if(e.getEventType()== NoteEvent.Type.APPARITION){
				if(aliveNoteByFreq(e.getNote().getIdNote())==null) {
					//Si une apparition est détectée et la note n'est pas active
					Note n = new Note(e.getNote().getIdNote(), outputID, outputID.translated(1));
					output().addNote(n);
					stillAlive.add(n);
							System.out.print("");

				}
			}else{

				Note noteAlive = aliveNoteByFreq(e.getNote().getIdNote());
				if(noteAlive!=null) {
					//Si une disparition est détectée et la note est active
					if(noteAlive.getDuration()>minDuration.getValue())
						stillAlive.remove(aliveNoteIdByFreq(e.getNote().getIdNote()));
				}
			}
		}

		for (Note n: stillAlive)
			n.setEnd(outputID.translated(1));


		return result;
	}

	protected Note aliveNoteByFreq(int idFreq){
		for(Note n:stillAlive)
			if(n.getNoteID() == idFreq)
				return n;
		return null;
	}
	
	protected int aliveNoteIdByFreq(int idFreq){
		for (int id = 0; id < stillAlive.size(); id++)
			if(stillAlive.get(id).getNoteID() == idFreq)
				return id;

		return -1;
	}

	/**
	 * À  redéfinir: Renvoie la liste des index dépendant de l'échantillon d'index inputID.
	 *
	 * @param inputBufferID index du buffer de l'échantillon
	 * @param inputID       index de l'échantillon qui est utilisé par ...
	 * @return La liste des indexs de sortie qui utilise inputID pour leur calcul.
	 */
	@Override
	protected ArrayList<Instant> idUsedBy(int inputBufferID, int inputID) {
		ArrayList<Instant> r = new ArrayList<>();
		r.add(Instant.fromIndex(inputID, this.input()));
		return r;
	}

	/**
	 * À  redéfinir: Renvoie, pour chaque buffer d'entrée la liste des index nécessaire au calcul de l'échantillon d'index outputID.
	 *
	 * @param outputID index de l'échantillon que l'on veut calculer
	 * @return Un tableau de même taille que getInputs(), et qui contient les liste des échantillons de chaque buffer de getInputs() nécessaire au calcul.
	 */
	@Override
	protected ArrayList<Instant>[] idNeededFor(int outputID) {
		ArrayList<Instant> r = new ArrayList<>();
		ArrayList<Instant>[] ret = new ArrayList[1];
		r.add(Instant.fromIndex(outputID, output()));
		ret[0]=r;
		return ret;
	}

	/**
	 * Renvoie le type du buffer de sortie
	 *
	 * @return La class des échantillones du buffer de sortie
	 */
	@Override
	protected Class getType() {
		return NoteList.class;
	}
	protected ProcessingOrder getProcessingOrder(){
		return ProcessingOrder.ASCENDING_ORDER;
	}

	public  TemporalBuffer<NoteEventList> input(){
		return (TemporalBuffer<NoteEventList>)super.input();
	}

	public NoteBuffer output(){
		return (NoteBuffer)super.output();
	}

	@Override
	protected void clean() {
		stillAlive.clear();
		super.clean();
	}
}
