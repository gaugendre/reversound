/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.package actions;
*/
package actions;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by Gabriel on 28/03/2014.
 */
public class ExpertModeAction extends AbstractAction {
	public static final String ACTIVE = "ExpertModeActive";

	/**
	 * Create the ExpertModeAction.
	 * @param default_value Is the expert mode enabled ?
	 */
	public ExpertModeAction(boolean default_value) {
		super("Mode expert");
		putValue(ACTIVE,default_value);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		putValue(ACTIVE, !((Boolean) getValue(ACTIVE)));
	}

	/* public void setExpertMode(Boolean enabled) {
		putValue(ACTIVE,enabled);
	}*/
}