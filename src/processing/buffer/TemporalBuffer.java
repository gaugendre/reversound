/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.buffer;

import generictools.Instant;

/**
 * Created by gaby on 27/03/14.
 */
public class TemporalBuffer <T> extends Buffer<T> {
	private int sampleRate = 0;
	private Instant originInstant =Instant.fromTime(0);


	/**
	 * Constructeur du buffer temporel
	 * @param arrayType	type de stockage
	 * @param sampleRate fréquence d'échantillonage en Hz
	 */
	public TemporalBuffer(Storage.Type arrayType, int sampleRate) {
		super(arrayType);
		this.sampleRate = sampleRate;
	}

	public TemporalBuffer(Storage.Type arrayType, int sampleRate, Instant originInstant) {
		this(arrayType, sampleRate);
		this.originInstant = originInstant;
	}

	/**
	 * Renvoie la fréquence d'échantillonage
	 * @return Fréquence d'échatillonage en Hz
	 */

	public int getSampleRate() {
		return sampleRate;
	}

	/**
	 * Renvoie la valeur de l'instant pour l'index 0
	 * @return Instant de l'origine des indexs
	 */
	public Instant getOriginInstant() {
		return originInstant;
	}

	/**
	 * Renvoie la valeur de l'échantillon à un instant donné
	 * @param time Temps en ms
	 * @return Valeur de l'échantillon
	 */
	public T get(float time){
		return get(Instant.fromTime(time).mapToIndex(this));
	}

	/**
	 * Renvoie la valeur de l'échantillon à un instant donné
	 * @param instant Instant
	 * @return Valeur de l'échantillon
	 */
	public T get(Instant instant){
		return get(instant.mapToIndex(this));
	}


}
