/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.buffer;

import conteneurs.*;
import generictools.Instant;

import java.util.ArrayList;

/** Created by gaby on 02/06/14.
 * Buffer qui sert à stocker des notes */
public class NoteBuffer extends TemporalBuffer<NoteList> implements ObservableObject.Listener{

	ArrayList<Note> notes = new ArrayList<>();

	public NoteBuffer(Storage.Type arrayType, int sampleRate, Instant initialFrame) {
		super(arrayType, sampleRate, initialFrame);
	}

	/** Permet d'ajouter une note au buffer de notes
	 * @param note la note à ajouter
	 * @return l'identifiant lié a l'ajout de la note */
	public int addNote(Note note){
		int id = notes.size();
		notes.add(note);

		note.setBuffer(this);
		note.setBufferID(id);

		note.addListener(this);
		NoteID noteID = new NoteID(note);

		for (int i = note.getStart().mapToIndex(this); i <= note.getEnd().mapToIndex(this); i++)
			add(i, noteID);

		return id;
	}

	/** Permet de retirer une note au buffer
	 * @param bufferID l'identifiant de la note (pour le buffer)
	 * @return true si ça a fonctionné, false sinon */
	public boolean removeNote(int bufferID){
		Note n = getNote(bufferID);
		if(n==null)
			return false;

		for (int i = n.getStart().mapToIndex(this); i <= n.getEnd().mapToIndex(this); i++) {
			get(i).remove(get(i).getIdById(n.getNoteID()));
		}

		n.removeListener(this);

		return true;
	}

	/** Permet d'ajouter une note à un instant particulier, caractérisé par son temps
	 * @param timeID le numero de l'instant
	 * @param noteID l'identifiant de la note */
	public void add(int timeID, NoteID noteID){
		if(get(timeID)==null)
			set(timeID, new NoteList());
		get(timeID).add(noteID);
	}

	public Note getNote(int noteID){
		if(noteID<0 || noteID>notes.size())
			return null;

		return notes.get(noteID);
	}



	@Override
	public void observableOjectEvent(ObservableObjectEvent e) {
		if(e.getEmmiter() instanceof Note){
			Note note = (Note)e.getEmmiter();

			int id = note.getStart().mapToIndex(this);
			while(isCorrectIndex(id) && id<= note.getEnd().mapToIndex(this) && get(id).getNoteById(note.getNoteID())==null){
				get(id).add(note);
				id++;
			}

			id = note.getStart().mapToIndex(this)-1;
			while(isCorrectIndex(id) && get(id).getNoteById(note.getNoteID())!=null){
				get(id).remove(note);
				id--;
			}

			id = note.getEnd().mapToIndex(this);
			while(isCorrectIndex(id) && get(id).getNoteById(note.getNoteID())==null){
				get(id).add(note);
				id--;
			}

			id = note.getEnd().mapToIndex(this)+1;
			while(isCorrectIndex(id) && get(id).getNoteById(note.getNoteID())!=null){
				get(id).remove(note);
				id++;
			}
		}
	}

	/** Renvoie le nombre de notes presentes dans le buffer */
	public int getNotesNbr(){
		return notes.size();
	}
}
