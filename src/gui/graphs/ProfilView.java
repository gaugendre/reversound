/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.graphs;

import conteneurs.Profil;

import java.util.ArrayList;


/**
 * Created by Gabriel on 01/04/2014.
 */
public class ProfilView extends AmpliFreqView<Profil>{


	ProfilView() {
		super( "Visualisateur de profil", Profil.class);
	}

	@Override
	protected AbstractObservableGraph getGenericGraph() {
		return new ProfilView();
	}

	protected void initView() {
		super.initView();
		currentBarChart.getXAxis().setTickLabelRotation(0);
		createCategory(8);
	}

	private void createCategory(int nbCategory){
		ArrayList<String> categories = new ArrayList<>(nbCategory);

		if(nbCategory>0)
			categories.add("Fonda");

		for (int i = 1; i < nbCategory; i++)
			categories.add("Har " + String.valueOf(i));


		setXAxisCategories(categories);
	}

	@Override
	protected float getAmplitude(int idCategory) {
		return toShow().getAmplitude(idCategory);
	}}

