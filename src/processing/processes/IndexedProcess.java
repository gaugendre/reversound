/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.processes;

import generictools.Instant;
import processing.buffer.Buffer;
import processing.buffer.BufferEvent;
import processing.buffer.TemporalBuffer;

import java.util.ArrayList;

public abstract class IndexedProcess <T> extends Process<T> {

	public IndexedProcess (String name, Buffer[] inputs, Buffer<T> output) {
		super(name, inputs, output);
	}
	public IndexedProcess(String name, Buffer input, Buffer<T> output) {
		super(name,input, output);
	}

	protected BufferEventProcessor initBufferEventProcessor(){

		switch (getProcessingOrder()){
			case ASCENDING_ORDER:
				return new BufferEventProcessor() {

					int[] lastInputIndex = {};


					@Override
					public void processBufferEvent(BufferEvent e, int updateID) {
						//Test de la possibilité du calcul sur l'échantillon en sortie suivant

						for (int i = 0; i < lastInputIndex.length; i++) {
							if(lastInputIndex[i]>inputs()[i].size()-1)
								return;
						}

						output().add(compute(Instant.fromIndex(output().size(), output())));


						ArrayList<Instant>[] idNeeded= idNeededFor(output().size());
						lastInputIndex = new int[idNeeded.length];
						for (int i = 0; i < idNeeded.length; i++)
							lastInputIndex[i] = idNeeded[i].get(idNeeded[i].size()-1).mapToIndex(inputs()[i]);

					}
				};
		}

		return new BufferEventProcessor() {
			@Override
			public void processBufferEvent(BufferEvent e, int eventUpdateID) {
				int bufferID = 0;
				for(int i =1; i<inputs.length;i++)
					if(inputs[i]==e.getSource()) {
						bufferID = i;
						break;
					}

				//Des indices d'entrées on été modifiés, il faut mettre à jour les échantillons de sortie concernés

				//SI AJOUT
				// SI SET remplacer isComputable par != null;

				for (Instant i : idUsedBy(e.getSampleID(), bufferID)) {
					Integer outputUpdateID = output.getUpdateID(i.mapToIndex(output()));
					int iOutput = i.mapToIndex(output());
					if (iOutput>=0 && isComputable(iOutput) && (outputUpdateID==null || eventUpdateID > outputUpdateID || (eventUpdateID < 0 && outputUpdateID > 0))) {
						output().set(iOutput, compute(i), updateID);
						updateID++;
					}
				}
			}
		};
	}

	private boolean isComputable(int outputID){

		ArrayList<Instant>[] needed = idNeededFor(outputID);

		if(needed.length==0)
			return false;

		for (int i = 0; i < needed.length; i++) {
			for (Instant j : needed[i])
				if (inputs[i].get(j.mapToIndex(inputs()[i])) == null)
					return false;
		}



		return true;
	}

	/**
	 * À  redéfinir: Retourne le résultat du calcul à placer à l'index outputID
	 * @param outputID index de l'échantillon que l'on veut calculer
	 * @return Résultat du calcul
	 */
	protected abstract T compute (Instant outputID);

	/**
	 * À  redéfinir: Renvoie la liste des index dépendant de l'échantillon d'index inputID.
	 * @param inputBufferID index du buffer de l'échantillon
	 * @param inputID index de l'échantillon qui est utilisé par ...
	 * @return La liste des indexs de sortie qui utilise inputID pour leur calcul.
	 */
	protected abstract ArrayList<Instant> idUsedBy (int inputBufferID, int inputID);

	/**
	 * À  redéfinir: Renvoie, pour chaque buffer d'entrée la liste des index nécessaire au calcul de l'échantillon d'index outputID.
	 * @param outputID index de l'échantillon que l'on veut calculer
	 * @return Un tableau de même taille que getInputs(), et qui contient les liste des échantillons de chaque buffer de getInputs() nécessaire au calcul.
	 */
	protected abstract ArrayList<Instant>[] idNeededFor (int outputID);


	public TemporalBuffer input(){
		return (TemporalBuffer)super.input();
	}


	public TemporalBuffer[] inputs(){
		TemporalBuffer[] r = new TemporalBuffer[super.inputs().length];
		for (int i = 0; i < super.inputs().length; i++) {
			r[i] = (TemporalBuffer)super.inputs()[i];
		}
		return r;
	}

	public TemporalBuffer<T> output(){
		return (TemporalBuffer<T>)super.output();
	}


}
