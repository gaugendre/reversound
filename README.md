reversound
==========

Reversound is an app that lets you generate the music sheet from an audio file.  
See https://reversound.augendre.info (fr) for more information.

# Reuse
If you do reuse my work, please consider linking back to this repository 🙂