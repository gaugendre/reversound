/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.properties;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.util.ArrayList;

/**
 * Created by gaby on 27/05/14.
 */
public class IntProperty extends Property<Integer> {
	ArrayList<TextField> textFields = new ArrayList<>();
	int min = Integer.MIN_VALUE;
	int max = Integer.MAX_VALUE;


	public IntProperty(String name, int value){
		super(name, Float.class, value);
	}

	public IntProperty(String name) {
		this(name, 0);
	}


	public IntProperty(String name, int value, int min, int max){
		this(name);
		setMin(min);
		setMax(max);
		setValue(value);
	}

	/**
	 * Modifie la valeur de la propriété en s'assurant qu'elle respecte les conditions de minimum et de maximum
	 * @param value la nouvelle valeur
	 * @return Renvoie vrai si l'opération s'est bien déroulé (si la valeur est dans l'intervale autorisé)
	 */
	public boolean setValue(Integer value){
		if(value>=min && value<=max)
			return super.setValue(value);

		return false;
	}

	/**
	 * Modifie la valeur maximale de l'intervale autorisé et modifie la valeur min si l'intervale est incorrect
	 * @param max nouvelle valeur max
	 */
	public void setMax(int max){
		this.max = max;
		if(min>max)
			min = max;
		if(getValue()>max)
			setValue(max);
	}

	/**
	 * Modifie la valeur minimale de l'intervale autorisé et modifie la valeur max si l'intervale est incorrect
	 * @param min nouvelle valeur min
	 */
	public void setMin(int min){
		this.min = min;
		if(min>max)
			max = min;
		if(getValue()<min)
			setValue(min);
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	/**
	 * Retourne l'élément javaFX de modification de la propriété
	 *
	 * @return L'élément graphique lié à la propriété
	 */
	@Override
	public Control createEditNode() {
		TextField textField = new TextField();
		textField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				int v = 0;
				try{
					v = Integer.parseInt(textField.getText());
				}catch (NumberFormatException e){
					textField.setText(getValue().toString());
					return;
				}

				if(v==getValue())
					return;

				if(v<min){
					textField.setText(String.valueOf(min));
					nodeValueModified(min);
					return;
				}
				if(v>max){
					textField.setText(String.valueOf(max));
					nodeValueModified(max);
					return;
				}

				nodeValueModified(v);
			}
		});
		textFields.add(textField);
		textField.setText(getValue().toString());
		return textField;
	}

	/**
	 * Appeler lorsque la proriété est modifier par autre chose que le l'élément graphique,
	 * il faut donc mettre à jour se dernier
	 */
	@Override
	protected void syncNodesValue() {
		for(TextField t: textFields)
			t.setText(getApparentValue().toString());
	}
}
