/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

import java.util.ArrayList;

/**
 * Created by gaby on 06/05/14.
 *
 * Objet écoutable
 *
 * Classe abstraite qui doit hérité de tout objet dont on veut pouvoir recevoir un évenement
 * lorsqu'il subit une modification.
 */
public abstract class ObservableObject {

	ArrayList<Listener> listeners = new ArrayList<>();

	public ObservableObject() {
	}

	/**
	 * Ajoute des écouteurs à l'objet écoutable
	 * @param l Objet écouteur
	 */
	public void addListener(Listener l){
		if(listeners.contains(l))
			return;

		listeners.add(l);
	}


	/**
	 * Supprime un écouteurs
	 * @param l écouteurs à supprimer
	 */
	public void removeListener(Listener l){
		if(!listeners.contains(l))
			return;
		listeners.remove(l);
	}

	/**
	 * Supprime un écouteur par sont index dans la liste des écouteurs
	 * @param idListener index des écouteurs
	 */
	public void removeListener(int idListener){
		if(idListener<0 && idListener>=listeners.size())
			return;
	}

	/**
	 * Supprime un écouteur selon sa signature
	 * @param signature signature de l'écouteur à supprimer
	 */
	public void removeSignedListener(String signature){
		int id = getSignedListenerID(signature);

		if(id==-1)
			return;
	removeListener(id);
	}

	/**
	 * Retourne l'index d'un écouteur dans la liste des écouteurs en fonction de se signature
	 * @param signature Signature de l'écouteur recherché
	 * @return Index de l'écouteur
	 */
	public int getSignedListenerID(String signature){
		for (int i = 0; i < listeners.size(); i++)
			if (listeners.get(i) instanceof SignedListener)
				if (((SignedListener) listeners.get(i)).signature().equals(signature))
					return i;

		return -1;
	}

	/**
	 * Transmet un évenement à tous les écouteurs
	 * @param e évenement à propager
	 */
	public void emit(ObservableObjectEvent e){
		for (int i = 0; i < listeners.size(); i++) {
			listeners.get(i).observableOjectEvent(e);
		}
	}

	/**
	 * Créé et transmet un évenement d'un type à tous les écouteurs
	 * @param t type de l'évenement à propager
	 */
	public void emit(ObservableObjectEvent.Type t){
		emit(new ObservableObjectEvent(this, t));
	}

	/**
	 * Interface définissant les objets écouteurs
	 */
	static public interface Listener {
		/**
		 * Traitement de l'évenement d'objet observable
		 * @param e evenement
		 */
		public void observableOjectEvent(ObservableObjectEvent e);
	}

	/**
	 * Classe abstraite définissant un objet écouteur auquelle on ajoute une signature pour pouvoir
	 * retrouver sans le stocké un écouteur grace à un String unique par écouteurs.
	 */
	static public abstract class SignedListener implements Listener{
		public void SignedListener(){
		}

		abstract public String signature();
	}

	public String toString(){
		return "Observable object";
	}

	/**
	 * Renvoie le type de l'objet écoutable
	 * @return Type de l'objet
	 */
	public abstract Class getType();
}

