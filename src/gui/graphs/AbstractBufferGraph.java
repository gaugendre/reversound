/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package gui.graphs;

import conteneurs.ObservableObject;
import gui.PlayerControl;
import gui.viewer_state.BufferViewerState;
import gui.viewer_state.Viewer;
import processing.buffer.Buffer;

/**
 * Created by gaby on 08/05/14.
 * Classe abstraite définissant les attributs et méthode communes au graph de buffer
 */
public abstract class AbstractBufferGraph <T extends Buffer> extends AbstractGraph implements Viewer, PlayerControl.Listener{
	private T buffer;
	private BufferViewerState state;

	public AbstractBufferGraph(String name, Class type) {
		super(name, type);
	}

	@Override
	public AbstractGraph setupGraph(Buffer b, GraphicView graphicView, boolean syncWithPlayer){
		AbstractBufferGraph g = getGenericGraph();
		g.setGraphicView(graphicView);
		g.setBuffer(b);

		if(syncWithPlayer) {
			PlayerControl.instance().addListener(g);
		}

		g.setupView();
		g.setBufferViewerState(BufferViewerState.continousWindow(g, b));
		return g;
	}

	@Override
	public AbstractGraph setupGraph(ObservableObject o, GraphicView graphicView) {
		return null;
	}

	/**
	 * Vérifie que le graph est valide (qu'il n'a pas été créé via un contructeur)
	 * @return Vrai si valide
	 */
	@Override
	public boolean isValid() {
		return buffer!=null;
	}

	@Override
	protected void visibilityChange(boolean isVisible) {
		state.setVisibility(isVisible);
	}

	abstract protected AbstractBufferGraph getGenericGraph();

	private void setBuffer(T buffer){this.buffer = buffer;}
	private void setBufferViewerState(BufferViewerState b){state = b;}

	public T buffer(){return buffer;}
	public BufferViewerState state(){return state;}
}
