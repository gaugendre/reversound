/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui;

import generictools.Pair;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;

/**
 * Classe générale permettant de gérer l'affichage des Process et des Inputs dans la fenêtre de contrôle.
 */
public class Box extends Group {

    protected final static int NODE_WIDTH = ProcessSumUp.NODE_WIDTH;
    protected final static int NODE_HEIGHT = ProcessSumUp.NODE_HEIGHT;
    protected final static int TEXT_LENGTH = ProcessSumUp.TEXT_LENGTH;

    private final Rectangle rectangle = new Rectangle(NODE_WIDTH, NODE_HEIGHT);
    protected final Text titre = new Text();

    private final ArrayList<Node> clickableElements = new ArrayList<>();

    private final Pair<Integer, Integer> coordinates = new Pair<>();
    private final ArrayList<ProcessBox> successorList = new ArrayList<>();

    public Box() {
        this(Color.AQUA, new Text("Inconnu"));
    }

    public Text getTitre() {
        return titre;
    }

    /**
     * Renvoie les éléments dont on souhaite qu'ils affichent la fenêtre lors d'un clic.
     * Il s'agit typiquement du rectangle de fond ainsi que du titre.
     * @return L'arraylist des éléments
     */
    public ArrayList<Node> getClickableElements() {
        return new ArrayList<>(clickableElements);
    }

    /**
     * Constructeur
     * @param C La couleur de la boîte.
     * @param T Le texte à afficher.
     */
    public Box(final Color C, final Text T) {
        super();

        //Initialisation de la boîte
        rectangle.setFill(C);
        rectangle.setArcHeight(10);
        rectangle.setArcWidth(10);

        DropShadow dS = new DropShadow();
        dS.setRadius(10);
        rectangle.setEffect(dS);


        //Initialisation du texte
        titre.setText(T.getText());
        titre.setFill(Color.BLACK);
        titre.setFont(Font.font("Arial", 15));
        titre.setX(20);
        titre.setY(24);

        //Troncature du texte si trop long
        if (titre.getText().length() > TEXT_LENGTH+3) {
            titre.setText(titre.getText().substring(0,TEXT_LENGTH)+"...");
        }

        this.getChildren().add(rectangle);
        this.getChildren().add(titre);

        //On indique quels sont les éléments cliquables
        clickableElements.add(rectangle);
        clickableElements.add(titre);
    }

    /**
     * Ajoute un successeur à la liste des successeurs de cette box.
     * @param s Le successeur à ajouter.
     */
    public void addSuccessor(final ProcessBox s) {
        this.successorList.add(s);
    }

    /**
     * Modifie les coordonnées de la boîte dans la grille.
     * @param m Abscisses (colonnes).
     * @param n Ordonnées (lignes).
     */
    private void setCoordinates(int m, int n) {
        coordinates.setFirst(m);
        coordinates.setSecond(n);
    }

    /**
     * Génère les coordonnées du/des successeurs.
     * @param c La coordonnée en abscisses de la boîte considérée.
     * @param l La coordonnée en ordonnées de la boîte considérée.
     * @param boxList La liste des successeurs.
     */
    public void generateSuccCoord(int c, int l, ArrayList<Box> boxList) {
        this.setCoordinates(c, l);
        if (this instanceof InputBox) {
            InputBox iBox = (InputBox) this;
            boxList.add(iBox);
        }
        if (this instanceof ProcessBox) {
            ProcessBox pBox = (ProcessBox) this;
            if (!pBox.getProcess().isBoxDisplayed()) {
                boxList.add(pBox);
                pBox.getProcess().setBoxDisplayed(true);
            }
        }
        for (int i = 0; i < getSuccessorList().size(); i++) {
            successorList.get(i).generateSuccCoord(c+1, l+i, boxList);
        }
    }

    /**
     * Renvoie les coordonnées de la boîte dans la grille.
     * @return Une paire d'entiers représentant les coordonnées. En premier les abscisses, en second les ordonnées.
     */
    public Pair<Integer, Integer> getCoordinates() {
        return coordinates;
    }

    /**
     * Renvoie la liste des successeurs.
     * @return Une ArrayList de ProcessBox représentant les successeurs de la boîte considérée.
     */
    public ArrayList<ProcessBox> getSuccessorList() {
        return successorList;
    }
}
