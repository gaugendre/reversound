/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.processes;

/**
 * Created by gaby on 25/05/14.
 * Caractérise les différents evenements qui peuvent intervenir sur les processus qui tournent */
public class ProcessEvent {

	//Les différents types d'évènements
	public  enum Type{
		PAUSED_STATE_CHANGED,
		WAITING_STATE,
		PROGRESS_CHANGED,
		NAME_CHANGED,
		INTERRUPT_AFTER
	}
	private Type type;
	private String name;
	private boolean isPaused;
	private boolean isWaiting;

	public ProcessEvent(Type type, String name, boolean isPaused, boolean isWaiting) {
		this.type = type;
		this.name = name;
		this.isPaused = isPaused;
		this.isWaiting = isWaiting;
	}

	/** Renvoie l'évenement lié au process donné en paramètre, en fonction du type donné en paramètre */
	public static ProcessEvent eventFrom(Type type, Process process){
		return new ProcessEvent(type, process.getName(), process.isPaused(), process.isWaiting());
	}

	public Type getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public boolean isPaused() {
		return isPaused;
	}

	public boolean isWaiting() {
		return isWaiting;
	}
}
