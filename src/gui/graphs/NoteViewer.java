/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.graphs;

import conteneurs.NoteList;
import generictools.Instant;
import javafx.scene.paint.Color;
import processing.buffer.NoteBuffer;

/**
 * Created by gaby on 03/06/14.
 */
public class NoteViewer extends BargramView<NoteBuffer> {
	public NoteViewer() {
		super("Tablature", NoteList.class);
	}

	@Override
	protected Color getColor(int idFreq, Instant time) {
		NoteList list = buffer().get(time);
		if(list == null)
			return null;

		if(list.getNoteById(idFreq)!=null)
			return Color.rgb(51,108,212);

		return null;
	}

	@Override
	protected AbstractBufferGraph getGenericGraph() {
		return new NoteViewer();
	}
}
