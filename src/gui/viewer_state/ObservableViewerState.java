/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui.viewer_state;

import conteneurs.ObservableObject;
import conteneurs.ObservableObjectEvent;

/**
 * Created by gaby on 07/05/14.
 *
 * Classe controlleur s'interposant entre les objets observables et leur visualisateur,
 * il gère les mises à jour différée dans le temps
 */
public class ObservableViewerState extends ViewerState implements ObservableObject.Listener {
	ObservableObject observableObject;

	public ObservableViewerState(Viewer viewer, ObservableObject observableObject) {
		super(viewer);
		setObservableObject(observableObject);
		init();
	}

	public ObservableViewerState(Viewer viewer){
		this(viewer, null);
	}

	@Override
	public void observableOjectEvent(ObservableObjectEvent e) {
		setState(State.UPDATE_NEEDED, slowingFactorOnDataChange());
	}

	@Override
	protected boolean canDraw() {
		return observableObject!=null;
	}

	public void setObservableObject(ObservableObject observableObject){
		if(this.observableObject!=null)
			this.observableObject.removeListener(this);

		this.observableObject = observableObject;
		if(this.observableObject!=null)
			this.observableObject.addListener(this);

		invalidate();
	}

	public ObservableObject getObservableObject() {
		return observableObject;
	}

	@Override
	protected void visibilityChanged() {
		if(isVisible()){
			observableObject.addListener(this);
		}else
			observableObject.removeListener(this);
	}
}
