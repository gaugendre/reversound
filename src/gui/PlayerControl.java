/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui;

import generictools.Instant;
import processing.BufferPlayer;
import processing.ProcessControl;
import processing.buffer.TemporalBuffer;

import java.util.ArrayList;

/**
 * Created by Gabriel on 12/03/14.
 * Controleur de lecture
 *
 * Il s'assure que toutes les vues soient synchronisées et est capable de démarrer, mettre en pause ou arrêter la lecture
 */

public class PlayerControl{

	public enum PlayingState{
		PLAYING,
		PAUSED,
		STOPPED
	}

	private PlayingState playingState;
	private Instant frame = new Instant(0);

    private final ArrayList<Listener> views = new ArrayList<>();

	private BufferPlayer player = new BufferPlayer(ProcessControl.instance().getMainInput().getBuffer());

	static private PlayerControl pC = new PlayerControl();

	/**
	 * Retourne l'instance du singleton du PlayerControl
	 * @return singleton du PlayerControl
	 */
	static public PlayerControl instance(){return pC;}

    private PlayerControl() {

    }

	/**
	 * Propage l'information "play" à travers toutes les vues controlées.
	 */
    public void play() {
		player.play();
		playingState = PlayingState.PLAYING;

		emit(PlayerControlEvent.Type.PLAY_STATE);
    }

	/**
	 * Propage l'information "play" à travers toutes les vues controlées.
	 * @param frame La frame depuis laquelle partir.
	 */
    public void play(Instant frame) {
		setFrame(frame);
		play();
    }

	/**
	 * Propage l'information "pause" à travers toutes les vues controlées.
	 */
    public void pause() {
		player.pause();
		playingState = PlayingState.PAUSED;

		emit(PlayerControlEvent.Type.PLAY_STATE);
    }

	/**
	 * Propage l'information "interrupt" à travers toutes les vues controlées.
	 */
    public void stop() {
		player.pause();
		setFrame(Instant.fromIndex(0,ProcessControl.instance().getMainInput().getBuffer()));
		playingState = PlayingState.STOPPED;
		emit(PlayerControlEvent.Type.PLAY_STATE);
    }

	/**
	 * Retourne l'état du player (si il est en lecture, en pause ou arrêté
	 * @return l'état du lecteur
	 */
	public PlayingState getPlayingState(){return playingState;}


	public void mute(){
		player.setMuted(true);
	}
	public void unMute(){
		player.setMuted(false);
	}
	public boolean isMute(){
		return player.isMuted();
	}

	/**
	 * Bascule l'état de mute: si muted unmute, sinon mute
	 */
	public void toggleMute(){
		if(isMute())
			unMute();
		else
			mute();
	}

	/**
	 * Modifie le volume du player
	 * @param volume 0 = mute, 1 = fullVolume, 2 = saturation permanente...
	 */
	public void setVolume(float volume){
		player.setVolume(volume);
	}

	public float getVolume(){
		return player.getVolume();
	}

	/**
	 * Force la position dans le buffer de référence, l'information est propagé à toutes les vues et lecteurs controlés
	 * @param frame
	 */
	public void setFrame(Instant frame){
		player.setFrame(frame);
	}

	/**
	 * Méthode appelée par le lecteur lorsque la position actuelle à changé
	 * @param frame nouvelle poition
	 */
	public void frameChanged(Instant frame){
		this.frame = frame;
		emit(PlayerControlEvent.Type.FRAME);
	}

	/**
	 * Renvoie la position actuelle dans le buffer de référence
	 * @return position actuelle
	 */
	public Instant getFrame(){return frame;}

	/**
	 * Raccourci statique de getFrame()
	 * @return position actuelle
	 */
	public static Instant frame(){return PlayerControl.instance().getFrame();}

	public void setBuffer(TemporalBuffer<Float> buffer){
		player = new BufferPlayer(buffer);
	}


	/**
	 * Ajoute une vue au PlayerControl.
	 */
	public boolean addListener(Listener listener) {
		if(views.contains(listener))
			return false;

		views.add(listener);
		return true;
	}

	/**
	 * Supprime une vue au PlayerControl
	 * @param listener vue à supprimer
	 * @return Faux si la vue n'était pas ajoutée
	 */
	public boolean removeListener(Listener listener){
		if(!views.contains(listener))
			return false;

		views.remove(listener);
		return true;
	}

	private void emit(PlayerControlEvent.Type type){
		PlayerControlEvent e = new PlayerControlEvent(type);
		for(Listener l: views)
			l.playerControlEvent(e);
	}


	/**
	 * Interface définissant les objets capable de recevoir les évenements de lecture
	 */
	public interface Listener{
		/**
		 * Traite l'évenement de lecture
		 * @param e évenement
		 */
		public default void playerControlEvent(PlayerControlEvent e) {
			switch (e.getType()){
				case FRAME:
					break;

				case PLAY_STATE:
					switch (e.getPlayingState()){
						case PLAYING:
							break;
						case PAUSED:
							break;
						case STOPPED:
							break;
					}
					break;

				case VOLUME:
					break;

				case MUTE_STATE:
					break;
			}
		}
	}

}
