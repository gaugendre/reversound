/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.package actions;
*/

package actions;

import generictools.Strings;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.io.File;

/**
 * Ask to user to open a File and start reading.
 */
public class ImportFileAction extends AbstractAction {
    public static final String FILE = "Fichier";
    private final JComboBox<String> sourceList;
    private final JFileChooser chooser = new JFileChooser();
    private final JFrame frame;

	public ImportFileAction(JFrame frame, JComboBox<String> sourceList){
	    super("Importer un fichier...");
        this.frame = frame;
        this.sourceList = sourceList;
    }

	/**
	 * Invoked when the user clicks either on file import button or menu item.
     * Loads the file in memory
	 * @param e An event.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier Audio wav", "wav");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(frame);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            if (sourceList.getItemCount() > 1)
                sourceList.removeItemAt(1);
            File audioFile = new File(chooser.getSelectedFile().getAbsolutePath());
            putValue(FILE, audioFile);
            sourceList.addItem(audioFile.getName());
            sourceList.setPrototypeDisplayValue(Strings.longestString("Fichier", audioFile.getName()));
            sourceList.setSelectedIndex(1);
        }
	}



}