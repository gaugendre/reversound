/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.properties;

import java.util.ArrayList;

/**
 * Created by gaby on 03/04/14.
 * Gestionnaire des propriétés. Permet de controler toutes les propriétés associées à chaque process.
 */
public class PropertiesManager {

	private final ArrayList<Property<?>> properties;

	public PropertiesManager(){
		properties = new ArrayList<>();
	}

	/** Permet d'ajouter une propriété */
	public int addProperty(Property<?> property){
        for (Property property1 : properties) {
            if (property.getName() == property1.getName())
                return -1;
        }
		properties.add(property);

		return properties.size()-1;
	}


	/** Récupère une propriété grâce à son identifiant */
	public Property<?> getProperty(int id){
		if(id >= properties.size() || id < 0)
			return null;

		return properties.get(id);
	}

	/** Renvoie la propriété donnée identifiée par son noim
	 * @param name le nom de la propriété
	 * @return la propriété */
	public Property getProperty(String name){
		return getProperty(getPropertyIndex(name));
	}

	public int getPropertyIndex(Property property){
		return properties.indexOf(property);
	}

	/** Renvoie l'indice de la propriété grâce à son nom
	 * @param name le nom de la propriété
	 * @return l'indice de la propriété */
	public int getPropertyIndex(String name){
		for (int i = 0; i < properties.size(); i++) {
			if(name.equals(properties.get(i).getName()))
				return i;
		}
		return -1;
	}
	
	/** Renvoie le nombre de propriétés ajoutées */
    public int getNbProperties() {
        return properties.size();
    }

	/** Permet de bloquer une propriété: la rend non modifiable pendant qu'un process l'utilise */
	public void lockProperty(){
		for (int i = 0; i < properties.size(); i++) {
			properties.get(i).lock();
		}
	}

	/** Permet de débloquer une propriété : la rend modifiable dès qu'elle n'est plus utilisée */
	public boolean unlockProperty(){
		boolean r = false;
		for (int i = 0; i < properties.size(); i++) {
			if(properties.get(i).unlock())
				r = true;
		}
		return r;
	}

}
