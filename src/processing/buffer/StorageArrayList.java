/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.buffer;


import java.util.ArrayList;

/**
 * Classe "adaptateur" entre les classiques ArrayList et les instances de Storage utilisable par Buffer.
 * Certaines méthodes de ArrayList ont été redéfinie, nottament get renvoie null si il n'y as pas d'éléments demandé (il ne provoque pas d'exeption).
 * @param <T> Le type des éléments contenu dans ce StorageArrayList
 */

public class StorageArrayList<T> extends ArrayList<T> implements Storage<T> {

	private int lastIndex=-1;
	int idIterator=-1;

	public StorageArrayList(){
		super();
	}

	/**
	 * Redéfinition de get(int) de ArrayList. La principale différence est de renvoyer null plutot que de lancer une exeption
	 * @param index index de l'élément désiré
	 * @param silent si vrai le rapport d'exeption ne sera pas affiché
	 * @return l'élément correspondant à l'index, ou null si aucun n'y était associé
	 */
	public T get(int index, boolean silent){

		if(index < 0 || index >=size())
			return null;

		return super.get(index);

	}

	public T get(int index){
		if(index < 0 || index >=size())
			return null;

		return super.get(index);
	}

	/**
	 * Redéfintion de set(int,T) de ArrayList. La principale différence est que l'index donné n'est pas obligé d'être &lt; size().
	 * La méthode s'occupera, si c'est le cas de compléter par des éléments null les indexs entre size()-1 et index
	 * @param index index de l'élément à modifier
	 * @param value nouvelle valeur de cet élément
	 * @return la valeur de l'élément précédant (null si aucun ne s'y trouvait).
	 */
	public T set(int index, T value){
		T previous = get(index);

		if(value==null)
			return remove(index);

		if(previous==null) {
			ensureSize(index + 1);
			lastIndex = index;
		}

		super.set(index, value);

		return previous;
	}

	/**
	 * Redéfintion de addNote(T) de ArrayList. Ajoute un élément après le dernier: à dernier index +1;
	 * @param value valeur de l'élément à ajouter
	 * @return Vrai si l'opération c'est bien passé (mais peut-elle rater?)...
	 */
	public boolean add(T value){

		set(lastIndex+1,value);

		return true;
	}

	/**
	 * Assure que la taille minimum de la liste est newSize (en complétant avec des éléments null)
	 * @param newSize la nouvelle taille minimum de la liste
	 */
    void ensureSize(int newSize){
		ensureCapacity(newSize);

		while(super.size()<newSize)
			super.add(null);
	}

	public T remove(int index){
		T r;
		try{
			r = super.remove(index);
		}catch(IndexOutOfBoundsException e){
			//e.printStackTrace();
			return null;
		}

		do {
			lastIndex--;
		}while(get(lastIndex)==null&&lastIndex>-1);
		return r;
	}


	public int size(){ return lastIndex+1;}

	public boolean isEmpty(){ return lastIndex==-1; }

	public ArrayList<Integer> getSamplesIndex(){

		ArrayList<Integer> r = new ArrayList<>();
		r.ensureCapacity(lastIndex);

		for (int i = 0; i < size(); i++)
			if(get(i)!=null)
				r.add(i);

		r.trimToSize();

		return r;
	}


	/**
	 * Génère un iterateur pointant initialement sur le première élément de la liste
	 *
	 * @return Un iterateur sur ce stockage
	 */
	@Override
	public StorageIterator<T> getIterator() {
		return getIterator(0);
	}

	/**
	 * Génère un iterateur pointant initialement sur l'élément de la liste correspondant à l'index en argument
	 *
	 * @param initialIndex index de l'élément sur lequel pointe initialement l'iterateur
	 * @return Un iterateur sur ce stockage
	 */
	@Override
	public StorageIterator<T> getIterator(final int initialIndex) {
		return new StorageIterator<T>() {
			private int currentID = initialIndex;
			@Override
			public T next() {
				if(currentID>=size())
					return null;

				return get(currentID++);
			}

			@Override
			public boolean hasNext() {
                return currentID < size();

            }
		};
	}

	/**
	 * Réserve directement un espace sans modifier la taille du tableau (si la forme si prête)
	 *
	 * @param capacity capacité maximale;
	 */
	@Override
	public void tryEnsureCapacity(int capacity) {
		ensureCapacity(capacity);
	}
}
