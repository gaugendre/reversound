/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package conteneurs;

/**
 * Created by gaby on 14/05/14.
 */
public class NoteEvent extends ObservableObject {
	private NoteProfiled note;
	private Type type;
	private float eventAmpli;

	public NoteEvent(NoteProfiled note, Type type, float eventAmpli) {
		this.note = note;
		this.type = type;
		this.eventAmpli = eventAmpli;
	}

	/** Permet de creer un evenement de type "Apparition de note"
	 * @param eventAmpli l'amplitude du changement
	 * @param note la note qui contient le profil */
	public static NoteEvent apparitionEvent(NoteProfiled note, float eventAmpli){
		return new NoteEvent(note, Type.APPARITION, eventAmpli);
	}

	/** Permet de creer un evenement de type "Disparition de note
	 * @param eventAmpli l'amplitude du changement
	 * @param note la note qui contient le profil */
	public static NoteEvent disparitionEvent(NoteProfiled note, float eventAmpli){
		return new NoteEvent(note, Type.DISPARITION, eventAmpli);
	}

	/** Renvoie la note associee a cette instance d'evenement */
	public NoteProfiled getNote() {
		return note;
	}

	/** Redefinit la note associee a cette instance d'evenement */
	public void setNote(NoteProfiled note) {
		this.note = note;
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
	}

	public float getEventAmpli() {
		return eventAmpli;
	}

	public void setEventAmpli(float eventAmpli) {
		this.eventAmpli = eventAmpli;
		emit(ObservableObjectEvent.Type.DATA_CHANGED);
	}

	/** Renvoie le type de l'evenement : Disparition ou Apparition */
	public Type getEventType(){
		return type;
	}

	@Override
	public Class getType() {
		return NoteEvent.class;
	}

	public enum Type{
		APPARITION,
		DISPARITION
	}

	public String toString(){
		return note.toString() + (type==Type.APPARITION?" Apparition: ":" Disparation: ") +eventAmpli;
	}
}
