/*
Reversound is used to get the music sheet of a piece from a music file.
Copyright (C) 2014  Gabriel AUGENDRE
Copyright (C) 2014  Gabriel DIENY
Copyright (C) 2014  Arthur GAUCHER
Copyright (C) 2014  Gabriel LEPETIT-AIMON

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package processing.buffer;


/**
 * Caractérise l'évennement envoyé par le buffer
 */

public class BufferEvent {
	private final int sampleID;
	private final Type type;
	private final Buffer bufferSource;



	public BufferEvent (Type eventType, int sampleID, Buffer bufferSource) {
		this.sampleID = sampleID;
		type = eventType;
		this.bufferSource = bufferSource;
	}

	/** Renvoie le type d'évenement */
	public Type getType () {
		return type;
	}

	/** Renvoie l'identifiant associé a l'échantillon en attribut */
	public int getSampleID () {
		return sampleID;
	}

	/** Renvoie le buffer source d'ou provient l'évenement */
	public Buffer getSource () {
		return bufferSource;
	}

	@Override
	public boolean equals(Object obj) {
        return obj instanceof BufferEvent && bufferSource == ((BufferEvent) obj).getSource();

    }

	public String toString(){
		String r = "";

		switch(type){
			case SAMPLE_ADDED:
				r+="Sample Added";
				break;
			case SAMPLE_CHANGED:
				r+="Sample Changed";
				break;
			case SAMPLE_DELETED:
				r+="Sample Deleted";
				break;
			case INVALIDATE:
				r+= "Invalidate processed samples";
				break;
			case INTERRUPT_PROCESS:
				r+="Interrupt process";
				break;
		}

		r+= ": " + sampleID;

		return r;
	}

	/** Définit les différents évemenements qui peuvent apparaitre dans un buffer */
	public enum Type {
		SAMPLE_ADDED,
		SAMPLE_CHANGED,
		SAMPLE_DELETED,
		INTERRUPT_PROCESS,
		INVALIDATE
	}

}

